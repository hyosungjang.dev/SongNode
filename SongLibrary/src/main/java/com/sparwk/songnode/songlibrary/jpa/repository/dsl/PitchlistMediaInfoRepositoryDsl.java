package com.sparwk.songnode.songlibrary.jpa.repository.dsl;

import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistMediaInfoBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface PitchlistMediaInfoRepositoryDsl {
    @Transactional
    Long PitchlistMediaInfoRepositoryDslDynamicUpdate(PitchlistMediaInfoBaseDTO dto );
}
