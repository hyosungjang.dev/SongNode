package com.sparwk.songnode.songlibrary.jpa.repository.dsl;

import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistRepository;
import org.springframework.transaction.annotation.Transactional;

public interface PitchlistRepositoryDsl {
    @Transactional
    Long PitchlistRepositoryDsl(PitchlistBaseDTO dto);
}

