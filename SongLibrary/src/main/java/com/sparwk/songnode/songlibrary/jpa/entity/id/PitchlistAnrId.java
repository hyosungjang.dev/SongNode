package com.sparwk.songnode.songlibrary.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PitchlistAnrId implements Serializable {

    private Long pitchlistAnrSeq;
    private Long pitchlistId;

}
