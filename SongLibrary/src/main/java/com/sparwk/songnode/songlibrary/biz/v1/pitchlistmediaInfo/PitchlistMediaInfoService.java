package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmediaInfo;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistmediaInfo.dto.PitchlistMediaInfoRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistMediaInfoBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistBaseServiceRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PitchlistMediaInfoService {


    @Autowired
    private PitchlistBaseServiceRepository pitchlistBaseServiceRepository;
//    @Autowired
//    private PitchlistMediaInfoRepository pitchlistMediaInfoRepository;
    @Autowired
    private ModelMapper modelMapper;

    private final Logger logger = LoggerFactory.getLogger(PitchlistMediaInfoService.class);

    public PitchlistMediaInfoBaseDTO PitchlistMediaInfoSaveService(PitchlistMediaInfoRequest dto) {
        PitchlistMediaInfoBaseDTO result = pitchlistBaseServiceRepository
                .PitchlistMediaInfoSaveService(modelMapper.map(dto, PitchlistMediaInfoBaseDTO.class));
        return result;
    }

    public List<PitchlistMediaInfoBaseDTO> PitchlistMediaInfoSelectService(Long pitchlistId) {
        List<PitchlistMediaInfoBaseDTO> result = pitchlistBaseServiceRepository.PitchlistMediaInfoSelectService(pitchlistId);
        return result;
    }

    public long PitchlistMediaInfoDynamicUpdateService(PitchlistMediaInfoRequest dto) {
        long result = pitchlistBaseServiceRepository
                .PitchlistMediaInfoDynamicUpdateService(modelMapper.map(dto, PitchlistMediaInfoBaseDTO.class));
        return result;
    }

}
