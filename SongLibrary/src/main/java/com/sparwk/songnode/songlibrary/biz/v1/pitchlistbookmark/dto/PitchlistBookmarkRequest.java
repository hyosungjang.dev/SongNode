package com.sparwk.songnode.songlibrary.biz.v1.pitchlistbookmark.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistBookmarkRequest {
    private Long profileId;
    private Long pitchlistId;
    private String bookmarkYn;
}
