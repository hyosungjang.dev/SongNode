package com.sparwk.songnode.songlibrary.config.filter;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class Token {

    private UserInfoDTO userInfoDTO;
}
