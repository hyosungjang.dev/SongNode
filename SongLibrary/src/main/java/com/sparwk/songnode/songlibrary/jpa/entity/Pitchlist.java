package com.sparwk.songnode.songlibrary.jpa.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist")
public class Pitchlist {

    @Id
    @GeneratedValue(generator = "tb_pitchlist_seq")
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "pitch_title", nullable = true)
    private String pitchTitle;
    @Column(name = "short_desc", nullable = true)
    private String shortDesc;
    @Column(name = "full_desc", nullable = true)
    private String fullDesc;
    @Column(name = "pitch_avatar_style_cd", nullable = true)
    private String pitchAvatarStyleCd;
    @Column(name = "avatar_img_path", nullable = true)
    private String avatarImgPath;
    @Column(name = "private_yn", nullable = true)
    private String privateYn;
    @Column(name = "private_pw", nullable = true)
    private String privatePw;
    @Column(name = "pitch_status", nullable = true)
    private String pitchStatus;
    @Column(name = "indivi_comp_yn", nullable = true)
    private String indiviCompYn;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "phon_no", nullable = true)
    private String phonNo;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "share_url", nullable = true)
    private String shareUrl;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Pitchlist(
        Long pitchlistId,
        Long profileId,
        String pitchTitle,
        String shortDesc,
        String fullDesc,
        String pitchAvatarStyleCd,
        String avatarImgPath,
        String privateYn,
        String privatePw,
        String pitchStatus,
        String indiviCompYn,
        String email,
        String phonNo,
        String delYn,
        String shareUrl,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
         this.pitchlistId = pitchlistId;
         this.profileId = profileId;
         this.pitchTitle = pitchTitle;
         this.shortDesc = shortDesc;
         this.fullDesc = fullDesc;
         this.pitchAvatarStyleCd = pitchAvatarStyleCd;
         this.avatarImgPath = avatarImgPath;
         this.privateYn = privateYn;
         this.privatePw = privatePw;
         this.pitchStatus = pitchStatus;
         this.indiviCompYn = indiviCompYn;
         this.email = email;
         this.phonNo = phonNo;
         this.delYn = delYn;
         this.shareUrl = shareUrl;
         this.regUsr                 =       regUsr;
         this.regDt                  =       regDt;
         this.modUsr                 =       modUsr;
         this.modDt                  =       modDt;
    }

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PitchlistBookmark.class)
//    @JoinColumn(name = "pitchlist_id", referencedColumnName = "pitchlist_id")
//    private List<PitchlistBookmark> pitchlistBookmark;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PitchlistMediaInfo.class)
//    @JoinColumn(name = "pitchlist_id", referencedColumnName = "pitchlist_id")
//    private List<PitchlistMediaInfo> pitchlistMediaInfo;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PitchlistMemb.class)
//    @JoinColumn(name = "pitchlist_id", referencedColumnName = "pitchlist_id")
//    private List<PitchlistMemb> pitchlistMemb;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PitchlistMetadata.class)
//    @JoinColumn(name = "pitchlist_id", referencedColumnName = "pitchlist_id")
//    private List<PitchlistMetadata> pitchlistMetadata;

}
