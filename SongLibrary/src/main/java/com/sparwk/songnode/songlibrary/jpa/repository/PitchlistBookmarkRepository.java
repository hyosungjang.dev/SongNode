package com.sparwk.songnode.songlibrary.jpa.repository;

import com.sparwk.songnode.songlibrary.jpa.entity.PitchlistBookmark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PitchlistBookmarkRepository extends JpaRepository<PitchlistBookmark, Long> {

    List<PitchlistBookmark> findByPitchlistId(Long PitchlistId);

}
