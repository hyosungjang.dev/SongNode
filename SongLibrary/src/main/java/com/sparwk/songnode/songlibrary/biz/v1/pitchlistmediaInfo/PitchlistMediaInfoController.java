package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmediaInfo;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistmediaInfo.dto.PitchlistMediaInfoRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistMediaInfoBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/pitchlistmedia")
@CrossOrigin("*")
@Api(tags = "PitchlistMedia Server")
public class PitchlistMediaInfoController {


    @Autowired
    private PitchlistMediaInfoService pitchlistMediaInfoService;
    private final Logger logger = LoggerFactory.getLogger(PitchlistMediaInfoController.class);

    @PostMapping(path = "/info")
    public ResponseEntity<PitchlistMediaInfoBaseDTO> PitchlistMediaInfoSaveController(
            @Valid @RequestBody PitchlistMediaInfoRequest dto) {
        PitchlistMediaInfoBaseDTO result = pitchlistMediaInfoService.PitchlistMediaInfoSaveService(dto);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/info/{pitchlistId}")
    public ResponseEntity<List<PitchlistMediaInfoBaseDTO>> PitchlistMediaInfoSelect(
            @PathVariable(name = "pitchlistId") Long pitchlistId, HttpServletRequest req) {
        List<PitchlistMediaInfoBaseDTO> result = pitchlistMediaInfoService.PitchlistMediaInfoSelectService(pitchlistId);
        return ResponseEntity.ok(result);
    }

    @PostMapping(path = "/info/update")
    public ResponseEntity<Long> PitchlistMediaInfoDynamicUpdateController(
            @Valid @RequestBody PitchlistMediaInfoRequest dto) {
        long result = pitchlistMediaInfoService.PitchlistMediaInfoDynamicUpdateService(dto);
        return ResponseEntity.ok(result);
    }


}
