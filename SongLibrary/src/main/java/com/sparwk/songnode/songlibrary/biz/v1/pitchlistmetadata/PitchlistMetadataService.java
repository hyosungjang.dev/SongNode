package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmetadata;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistmetadata.dto.PitchlistMetadataRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistMetadataBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistBaseServiceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PitchlistMetadataService {

    @Autowired
    private PitchlistBaseServiceRepository pitchlistBaseServiceRepository;
//    @Autowired
//    private PitchlistMetadataRepository pitchlistMetadataRepository;
    @Autowired
    private ModelMapper modelMapper;


    public PitchlistMetadataBaseDTO PitchlistMetadataSaveService(PitchlistMetadataRequest dto) {

        PitchlistMetadataBaseDTO result = pitchlistBaseServiceRepository
                .PitchlistMetadataSaveService(modelMapper.map(dto, PitchlistMetadataBaseDTO.class));
        return result;
    }

    public List<PitchlistMetadataBaseDTO> PitchlistMetadataSelectService(Long pitchlistId) {

        List<PitchlistMetadataBaseDTO>  result = pitchlistBaseServiceRepository
                .PitchlistMetadataSelectService(pitchlistId);
        return result;
    }

}
