package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmediaInfo.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistMediaInfoRequest {

    private Long pitchlistId;
    private Long mediaSeq;
    private String mediaTypeCd;
    private String mediaUrl;
    private String useYn;

}
