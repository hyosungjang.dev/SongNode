package com.sparwk.songnode.songlibrary.jpa.repository;


import com.sparwk.songnode.songlibrary.jpa.entity.Pitchlist;
import com.sparwk.songnode.songlibrary.jpa.repository.dsl.PitchlistRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PitchlistRepository extends JpaRepository<Pitchlist, Long>, PitchlistRepositoryDsl {

    List<Pitchlist> findByProfileId(Long profileId);

}
