package com.sparwk.songnode.songlibrary.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistMetadataBaseDTO {
    private Long pitchlistId;
    private String attrTypeCd;
    private String attrDtlCd;
}
