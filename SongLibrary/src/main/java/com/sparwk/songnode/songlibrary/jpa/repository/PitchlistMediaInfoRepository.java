package com.sparwk.songnode.songlibrary.jpa.repository;

import com.sparwk.songnode.songlibrary.jpa.entity.PitchlistBookmark;
import com.sparwk.songnode.songlibrary.jpa.entity.PitchlistMediaInfo;
import com.sparwk.songnode.songlibrary.jpa.repository.dsl.PitchlistMediaInfoRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PitchlistMediaInfoRepository extends JpaRepository<PitchlistMediaInfo, Long>, PitchlistMediaInfoRepositoryDsl {
    List<PitchlistMediaInfo> findByPitchlistId(Long PitchlistId);
}
