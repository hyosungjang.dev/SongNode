package com.sparwk.songnode.songlibrary.jpa.entity;


import com.sparwk.songnode.songlibrary.jpa.entity.id.SonglibCowriterSongId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_songlib_cowriter_song")
@IdClass(SonglibCowriterSongId.class)
public class songlibCowriterSong {
    @Id
    @Column(name = "songlib_cowriter_seq")
    private Long songlibCowriterSeq;
    @Column(name = "song_id", nullable = false)
    private Long songId;

    @Builder
    songlibCowriterSong(
        Long songlibCowriterSeq,
        Long songId
    ) {
         this.songlibCowriterSeq = songlibCowriterSeq;
         this.songId = songId;
    }

}
