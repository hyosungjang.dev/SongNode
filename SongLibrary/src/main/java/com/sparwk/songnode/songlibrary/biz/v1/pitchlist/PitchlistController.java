package com.sparwk.songnode.songlibrary.biz.v1.pitchlist;


import com.sparwk.songnode.songlibrary.biz.v1.pitchlist.dto.PitchlistRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/pitchlist")
@CrossOrigin("*")
@Api(tags = "Pitchlist Server")
public class PitchlistController {

    @Autowired
    private PitchlistService pitchlistService;
    private final Logger logger = LoggerFactory.getLogger(PitchlistController.class);

    @PostMapping(path = "/info")
    public ResponseEntity<PitchlistBaseDTO> PitchlistSaveController(@Valid @RequestBody PitchlistRequest dto) {
        PitchlistBaseDTO result = pitchlistService.PitchlistSaveService(dto);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/info/{profileId}")
    public ResponseEntity<List<PitchlistBaseDTO>> PitchlistSelect(
            @PathVariable(name = "profileId") Long profileId, HttpServletRequest req) {
        List<PitchlistBaseDTO> result = pitchlistService.PitchlistSelectService(profileId);
        return ResponseEntity.ok(result);
    }

    @PostMapping(path = "/info/update")
    public ResponseEntity<Long> PitchlistDynamicUpdateController(@Valid @RequestBody PitchlistRequest dto) {
        long result = pitchlistService.PitchlistDynamicUpdateService(dto);
        return ResponseEntity.ok(result);
    }


}
