package com.sparwk.songnode.songlibrary.jpa.repository;


import com.sparwk.songnode.songlibrary.jpa.entity.PitchlistBookmark;
import com.sparwk.songnode.songlibrary.jpa.entity.PitchlistMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PitchlistMetadataRepository extends JpaRepository<PitchlistMetadata, Long> {
    List<PitchlistMetadata> findByPitchlistId(Long PitchlistId);
}
