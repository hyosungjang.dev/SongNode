package com.sparwk.songnode.songlibrary.jpa.entity.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class PitchlistBookmarkId implements Serializable {

    private Long profileId;
    private Long pitchlistId;

}
