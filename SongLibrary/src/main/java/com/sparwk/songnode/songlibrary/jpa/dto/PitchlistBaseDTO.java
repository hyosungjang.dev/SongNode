package com.sparwk.songnode.songlibrary.jpa.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistBaseDTO {

    private Long pitchlistId;
    private Long profileId;
    private String pitchTitle;
    private String shortDesc;
    private String fullDesc;
    private String pitchAvatarStyleCd;
    private String privateYn;
    private String privatePw;
    private String pitchAvalYn;
    private String indiviCompYn;
    private String email;
    private String phonNo;
    private String delYn;

}
