package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmetadata.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistMetadataRequest {
    private Long pitchlistId;
    private String attrTypeCd;
    private String attrDtlCd;
}
