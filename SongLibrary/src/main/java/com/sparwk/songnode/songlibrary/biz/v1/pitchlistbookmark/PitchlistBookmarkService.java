package com.sparwk.songnode.songlibrary.biz.v1.pitchlistbookmark;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistbookmark.dto.PitchlistBookmarkRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBookmarkBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistBaseServiceRepository;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistBookmarkRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PitchlistBookmarkService {

    @Autowired
    private PitchlistBaseServiceRepository pitchlistBaseServiceRepository;
//    @Autowired
//    private PitchlistBookmarkRepository pitchlistbookmarkRepository;
    @Autowired
    private ModelMapper modelMapper;


    public PitchlistBookmarkBaseDTO PitchlistBookmarkSaveService(PitchlistBookmarkRequest dto) {

        PitchlistBookmarkBaseDTO result = pitchlistBaseServiceRepository
                .PitchlistBookmarkSaveService(modelMapper.map(dto, PitchlistBookmarkBaseDTO.class));
        return result;
    }

    public List<PitchlistBookmarkBaseDTO> PitchlistBookmarkSelectService(Long pitchlistId) {
        List<PitchlistBookmarkBaseDTO> result = pitchlistBaseServiceRepository.PitchlistBookmarkSelectService(pitchlistId);
        return result;
    }

}
