package com.sparwk.songnode.songlibrary.jpa.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistMediaInfoBaseDTO {

    private Long pitchlistId;
    private Long mediaSeq;
    private String mediaTypeCd;
    private String mediaUrl;
    private String useYn;

}
