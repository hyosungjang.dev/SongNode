package com.sparwk.songnode.songlibrary.jpa.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistCompanyBaseDTO {

    private Long pitchlistId;
    private String compCd;

}
