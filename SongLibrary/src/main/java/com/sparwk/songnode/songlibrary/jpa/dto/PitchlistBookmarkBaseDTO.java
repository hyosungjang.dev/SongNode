package com.sparwk.songnode.songlibrary.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistBookmarkBaseDTO {
    private Long profileId;
    private Long pitchlistId;
    private String bookmarkYn;
}
