package com.sparwk.songnode.songlibrary.jpa.entity;


import com.sparwk.songnode.songlibrary.jpa.entity.id.PitchlistMediaInfoId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_media_info")
@IdClass(PitchlistMediaInfoId.class)
public class PitchlistMediaInfo {
    @Id
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Id
    @GeneratedValue(generator = "tb_pitchlist_media_info_seq")
    @Column(name = "media_seq", nullable = true)
    private Long mediaSeq;
    @Column(name = "sns_type_cd", nullable = true)
    private String snsTypeCd;
    @Column(name = "sns_url", nullable = true)
    private String snsUrl;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", updatable = false)
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    private LocalDateTime modDt;

    @Builder
    PitchlistMediaInfo(
            Long pitchlistId,
            Long mediaSeq,
            String snsTypeCd,
            String snsUrl,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.pitchlistId=pitchlistId;
        this.mediaSeq=mediaSeq;
        this.snsTypeCd=snsTypeCd;
        this.snsUrl=snsUrl;
        this.useYn=useYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }
}
