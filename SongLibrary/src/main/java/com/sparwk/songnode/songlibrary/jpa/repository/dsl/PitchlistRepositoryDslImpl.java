package com.sparwk.songnode.songlibrary.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.entity.QPitchlist;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class PitchlistRepositoryDslImpl implements PitchlistRepositoryDsl {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(PitchlistRepositoryDslImpl.class);
    private QPitchlist qPitchlist = QPitchlist.pitchlist;

    private PitchlistRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long PitchlistRepositoryDsl(PitchlistBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qPitchlist);

        if (entity.getPitchTitle() == null || entity.getPitchTitle().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.pitchTitle, entity.getPitchTitle());
        }
        if (entity.getShortDesc() == null || entity.getShortDesc().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.shortDesc, entity.getShortDesc());
        }
        if (entity.getFullDesc() == null || entity.getFullDesc().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.fullDesc, entity.getFullDesc());
        }
        if (entity.getPitchAvatarStyleCd() == null || entity.getPitchAvatarStyleCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.pitchAvatarStyleCd, entity.getPitchAvatarStyleCd());
        }
        if (entity.getPrivateYn() == null || entity.getPrivateYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.privateYn, entity.getPrivateYn());
        }
        if (entity.getPrivatePw() == null || entity.getPrivatePw().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.privatePw, entity.getPrivatePw());
        }
        if (entity.getIndiviCompYn() == null || entity.getIndiviCompYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.indiviCompYn, entity.getIndiviCompYn());
        }
        if (entity.getEmail() == null || entity.getEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.email, entity.getEmail());
        }
        if (entity.getPhonNo() == null || entity.getPhonNo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.phonNo, entity.getPhonNo());
        }
        if (entity.getDelYn() == null || entity.getDelYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qPitchlist.delYn, entity.getDelYn());
        }

        jpaUpdateClause.where(qPitchlist.pitchlistId.eq(entity.getPitchlistId()));
        jpaUpdateClause.where(qPitchlist.profileId.eq(entity.getProfileId()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
