package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongCreditInstrument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCreditInstrumentRepository extends JpaRepository<SongCreditInstrument, Long> {

    List<SongCreditInstrument> findSongCreditInstrumentsBySongCreditSeq(Long songCreditSeq);
    Long deleteSongCreditInstrumentsBySongCreditSeq(Long songCreditSeq);
}
