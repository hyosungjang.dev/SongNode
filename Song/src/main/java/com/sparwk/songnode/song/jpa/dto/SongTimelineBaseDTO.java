package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongTimelineBaseDTO {
    private Long timelineSeq;
    private Long songId;
    private Long componentUseId;
    private String componetCd;
    private String tlmCd;
    private List<String> params;
    private String resultMsg;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
