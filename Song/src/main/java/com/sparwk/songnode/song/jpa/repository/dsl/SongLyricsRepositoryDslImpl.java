package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.jpa.entity.QSongLyrics;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class SongLyricsRepositoryDslImpl implements SongLyricsRepositoryDsl {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(SongLyricsRepositoryDslImpl.class);
    private QSongLyrics qSongLyrics = QSongLyrics.songLyrics;

    private SongLyricsRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long songLyricsRepositoryDynamicUpdate(SongLyricsDto entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongLyrics);

        //가사 text인데 어떻게 들어가려나?
        if(!StringUtils.isEmpty(entity.getLyrics())){
            jpaUpdateClause.set(qSongLyrics.lyrics, entity.getLyrics());
        }

        // 코멘트
        if(!StringUtils.isEmpty(entity.getLyricsComt())){
            jpaUpdateClause.set(qSongLyrics.lyricsComt, entity.getLyricsComt());
        }

        // 음란여부
        if(!StringUtils.isEmpty(entity.getExplicitContentYn())){
            jpaUpdateClause.set(qSongLyrics.explicitContentYn, entity.getExplicitContentYn());
        }

        if(entity.getSongCommonParam().getProfileId() != null){
            jpaUpdateClause.set(qSongLyrics.modUsr, entity.getSongCommonParam().getProfileId());
            jpaUpdateClause.set(qSongLyrics.modDt, LocalDateTime.now());
        }

        jpaUpdateClause.where(qSongLyrics.songId.eq(entity.getSongId()));
        jpaUpdateClause.where(qSongLyrics.songFileSeq.eq(entity.getSongFileSeq()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
