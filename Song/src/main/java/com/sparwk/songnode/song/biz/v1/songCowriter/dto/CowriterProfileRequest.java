package com.sparwk.songnode.song.biz.v1.songCowriter.dto;


import com.sparwk.songnode.song.constants.CommonCodeConst;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CowriterProfileRequest {

    //seq id 검색 후 처리
    @Schema(description = "참여할 song Id")
    private Long songId;
    @Schema(description = "ownerProfileId")
    private Long ownerProfileId;
    @Schema(description = "profileId")
    private Long profileId;

    @Schema(description = "copyrightControlYn")
    private String copyrightControlYn = CommonCodeConst.YN.N;
    @Schema(description = "person type")
    private String personIdType;
    @Schema(description = "person number")
    private String personIdNumber;
    @Schema(description = "Original Publisher Id")
    private Long opProfileId;
    @Schema(description = "PRO profileId")
    private Long proProfileId;

    @Schema(description = "member role")
    private String roleCdList;
    @Schema(description = "sub Publisher")
    private String spProfileIdList;

}
