package com.sparwk.songnode.song.biz.v1.song.dto;

import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterResponse;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongCreditBaseDTO;
import com.sparwk.songnode.song.jpa.dto.SongMetadataBaseDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongResponse {

    @Schema(description = "송 아이디")
    private Long songId;
    @Schema(description = "송 원본 ID")
    private Long originalSongId;
    @Schema(description = "프로필 아이디 /나중에 토큰에서 정보받아오는걸로 수정후 삭제예정")
    private Long profileId;
    @Schema(description = "송 아이디")
    private String songVersionCd;
    @Schema(description = "노래 소유자")
    private Long songOwner;
    @Schema(description = "언어")
    private String langCd;
    @Schema(description = "노래 제목")
    private String songTitle;
    @Schema(description = "노래 부제목")
    private String songSubTitle;
    @Schema(description = "노래 설명")
    private String description;
    @Schema(description = "아바타이미지 사용여부")
    private String avatarImgUseYn;
    @Schema(description = "아바타 이미지 경로")
    private String avatarFileUrl;
    @Schema(description = "노래 이용 상태")
    private String songAvailYn;
    @Schema(description = "노래 진행 상태 코드")
    private String songStatusCd;
    @Schema(description = "노래 상태 변경 일자")
    private LocalDateTime songStatusModDt;
    @Schema(description = "사용자 정의 버전")
    private String userDefineVersion;

    @Schema(description = "등록 user")
    private Long regUsr;
    @Schema(description = "등록날짜")
    private LocalDateTime regDt;
    @Schema(description = "수정 user")
    private Long modUsr;
    @Schema(description = "수정날짜")
    private LocalDateTime modDt;

    @Schema(description = "북마크 Y/N")
    private String bookmarkYn = CommonCodeConst.YN.N;

    @Schema(description = "song metadata")
    private List<SongMetadataBaseDTO> songMetadataBaseDTOs;

    @Schema(description = "song cowriterResponses")
    private List<CowriterResponse> cowriterResponses;

    @Schema(description = "song credit")
    private List<SongCreditBaseDTO> songCreditResponses;

    @Schema(description = "song file")
    private List<SongFileDto> songFileDtos;

//    @Schema(description = "song CowriterList")
//    private String songCowriterList;

}
