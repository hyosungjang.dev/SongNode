package com.sparwk.songnode.song.biz.v1.SongCredit.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCreditDto {
    @Schema(description = "노래 아이디")
    private Long songId;
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "삭제 여부")
    private String deleteYn;

    private Long modUsr;
    private LocalDateTime modDt;

    private Long songCreditSeq;
    private String ipnNumber;
    private Long nroProfileId;
    private String nroNumber;
}
