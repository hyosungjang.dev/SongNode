package com.sparwk.songnode.song.biz.v1.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCommonParam {

    @Schema(description = "프로필 아이디 /나중에 토큰에서 정보받아오는걸로 수정후 삭제예정")
    private Long profileId;

    @Schema(description = "songId")
    private Long songId;
}
