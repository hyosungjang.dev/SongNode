package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataRequest {

    private Long songId;
    private Long profileId;

    @Schema(description = "메타데이터 타입")
    private String genresCd;
    @Schema(description = "장르 리스트")
    private String genresList;

    @Schema(description = "메타데이터 타입")
    private String subGenresCd;
    @Schema(description = "서브장르 리스트")
    private String subGenresList;

    @Schema(description = "메타데이터 타입")
    private String themesCd;
    @Schema(description = "테마리스트")
    private String themesList;

    @Schema(description = "메타데이터 타입")
    private String moodsCd;
    @Schema(description = "무드리스트")
    private String moodsList;

    @Schema(description = "메타데이터 타입")
    private String vocalCd;
    @Schema(description = "보컬리스트")
    private String vocalList;

    @Schema(description = "메타데이터 타입")
    private String eraCd;
    @Schema(description = "era리스트")
    private String eraList;

    @Schema(description = "메타데이터 타입")
    private String tempoCd;
    @Schema(description = "tempo 리스트")
    private String tempoList;

    @Schema(description = "메타데이터 타입")
    private String instrumentsCd;
    @Schema(description = "instruments리스트")
    private String instrumentsList;
}