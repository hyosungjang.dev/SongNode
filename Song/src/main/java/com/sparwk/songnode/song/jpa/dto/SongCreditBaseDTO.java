package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCreditBaseDTO{
    private Long songId;
    private Long profileId;
    private String deleteYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private Long songCreditSeq;
    private String ipnNumber;
    private Long nroProfileId;
    private String nroNumber;
}
