package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataDto {
    @Schema(description = "노래 아이디")
    private Long songId;
    @Schema(description = "메타데이터 타입")
    private String attrTypeCd;
    @Schema(description = "메타 데이터 코드")
    private String attrDtlCd;

}
