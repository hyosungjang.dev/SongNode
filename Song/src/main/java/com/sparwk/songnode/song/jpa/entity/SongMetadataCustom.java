package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongMetadataCustomId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_metadata_custom")
@IdClass(SongMetadataCustomId.class)
public class SongMetadataCustom extends BaseEntity {

    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @GeneratedValue(generator = "tb_song_metadata_custom_seq")
    @Column(name = "metadata_seq", nullable = true)
    private Long metadataSeq;
    @Column(name = "metadata_name", nullable = true)
    private String metadataName;
    @Column(name = "metadata_value", nullable = true)
    private String metadataValue;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    SongMetadataCustom(
            Long songId,
            Long metadataSeq,
            String metadataName,
            String metadataValue,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
            ) {
        this.songId                 =       songId;
        this.metadataSeq            =       metadataSeq;
        this.metadataName           =       metadataName;
        this.metadataValue          =       metadataValue;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }
}
