package com.sparwk.songnode.song.biz.v1.projectSong.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongListResponse {

    private Long projId;
    private Long profileId;
    private int songCnt;
}
