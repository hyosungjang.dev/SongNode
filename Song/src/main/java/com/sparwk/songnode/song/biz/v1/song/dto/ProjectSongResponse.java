package com.sparwk.songnode.song.biz.v1.song.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongResponse {
    private Long projId;
    private Long projOwner;
    private Long songId;

}
