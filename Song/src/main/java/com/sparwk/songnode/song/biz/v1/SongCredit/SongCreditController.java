package com.sparwk.songnode.song.biz.v1.SongCredit;

import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditProfileRequest;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditRequest;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Environment.API_VERSION + "/credit")
@CrossOrigin("*")
@Api(tags = "Song Server SongCredit")
@RequiredArgsConstructor
public class SongCreditController {

    private final SongCreditDomain songCreditDomain;
    private final Logger logger = LoggerFactory.getLogger(SongCreditController.class);

    @ApiOperation(
            value = "Song credit set profile"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/set/profile")
    public Response<Void> updateCreditProfile(
            @RequestBody SongCreditProfileRequest songCreditProfileRequest
            ) {
        logger.info("#### initSongSeq #### {}", songCreditProfileRequest);
        return songCreditDomain.setProfileCredit(songCreditProfileRequest);
    }

    @ApiOperation(
            value = "Song Credit save"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/save")
    public Response<Void> saveSongCredit(
            @RequestBody SongCreditRequest songCreditRequest) {
        logger.info("#### saveSongCredit ####");
        return songCreditDomain.saveCredit(songCreditRequest);
    }

    @ApiOperation(
            value = "Song Credit delete"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/delete")
    public Response<Void> deleteSongCredit(
            @RequestBody SongCreditRequest songCreditRequest
    ) {
        logger.info("#### deleteSongCredit ####");
        return songCreditDomain.deleteCowriter(songCreditRequest);
    }
}
