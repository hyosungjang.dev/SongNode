package com.sparwk.songnode.song.biz.v1.songFile;

import com.sparwk.songnode.song.biz.v1.songLyrics.SongLyricsService;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.song.dto.SongVersionRequest;
import com.sparwk.songnode.song.jpa.SongServiceBaseRepository;
import com.sparwk.songnode.song.jpa.dto.SongFileBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongFile;
import com.sparwk.songnode.song.jpa.repository.SongFileRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongFileService {

    /**
     * service
     */
    private final SongLyricsService songLyricsService;

    /**
     * repository
     */
    private final SongFileRepository songFileRepository;
    private final SongServiceBaseRepository songServiceBaseRepository;
    private final ModelMapper modelMapper;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(SongFileDomain.class);


    /**
     * file save method
     * @param dto
     * @return
     */
    @Transactional
    public SongFileBaseDTO songFileSaveService(SongFileDto dto) {
        SongFile entity = modelMapper.map(dto, SongFile.class);
        entity.setRegUsr(Long.valueOf(1));
        entity.setRegDt(LocalDateTime.now());
        entity.setModUsr(Long.valueOf(1));
        entity.setModDt(LocalDateTime.now());
        entity = songFileRepository.save(entity);
        SongFileBaseDTO result = modelMapper.map(entity, SongFileBaseDTO.class);
        return result;
    }

    /**
     * song file list
     * @param songId
     * @return
     */
    public List<SongFileDto> songFileSelectService(Long songId) {
        List<SongFileDto> result = null;
        try {
            result = songFileRepository.findSongFilesBySongId(songId)
                    .stream().map(e -> modelMapper.map(e, SongFileDto.class)).collect(Collectors.toList());
        } catch (Exception e){
            logger.error("##### file select Error NOT VALID PARAMETER##### ");
        }
        return result;
    }


    /**
     * SONG FILE COPY
     * @param vo
     * @return
     */
    @Transactional
    public SongFileBaseDTO songFileCopy(SongVersionRequest vo) {
        // list 조회
        List<SongFileDto> list = songFileSelectService(vo.getSongId());
        SongFileBaseDTO result = null;
        if(list.size() > 0){
            // 새로 생성할 newSongId, newSongFilePath
            list.get(0).setSongId(vo.getNewSongId());
            list.get(0).setSongFilePath(vo.getNewSongFilePath());

            SongFile entity = modelMapper.map(list.get(0), SongFile.class);

            entity.setRegUsr(Long.valueOf(1));
            entity.setRegDt(LocalDateTime.now());
            entity.setModUsr(Long.valueOf(1));
            entity.setModDt(LocalDateTime.now());

            entity = songFileRepository.save(entity);
            result = modelMapper.map(entity, SongFileBaseDTO.class);

            if(result != null){
                // song lyrics
                SongLyricsDto songLyricsDto = songLyricsService.songLyricsSelectService(vo.getSongId(), list.get(0).getSongFileSeq());
                songLyricsService.songLyricsCopy(vo, songLyricsDto, result);
            }
        }

        return result;
    }


    /**
     * modify song file
     * @param dto
     * @return
     */
    @Transactional
    public void songFileDynamicUpdate(SongFileDto dto) {
        logger.info(" ##### ENTER songFileDynamicUpdate #####");
        try {
            Long result = songFileRepository.songFileRepositoryDynamicUpdate(dto);
        } catch (Exception e) {
            logger.error("#### SONG FILE UPDATE ERROR ####", e.getMessage());
        }
    }

    @Transactional
    public void deleteSongFileAll(Long songId){
        List<SongFileDto> files = songFileSelectService(songId);
        if(files.size() > 0) {
            Long fileSeq = files.get(0).getSongFileSeq();
            songLyricsService.deleteSongLyricsAll(songId,fileSeq);
            songFileRepository.deleteSongFilesBySongIdAndSongFileSeq(songId, fileSeq);
        }
    }

}
