package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.song.biz.v1.song.dto.SongResponse;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongBaseDTO;
import com.sparwk.songnode.song.jpa.entity.QProjectSong;
import com.sparwk.songnode.song.jpa.entity.QSong;
import com.sparwk.songnode.song.jpa.entity.QSongBookmark;
import com.sparwk.songnode.song.jpa.entity.QSongCowriter;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;

@Aspect
public class SongRepositoryDslImpl implements SongRepositoryDsl {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(SongRepositoryDslImpl.class);
    private QSong qSong = QSong.song;

    private SongRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }


    @Override
    public List<SongResponse> selectSongList(Long projId, Long profileId ,int offset, int limit) {

        QSong qSong = QSong.song;
        QProjectSong qProjectSong = QProjectSong.projectSong;
        QSongBookmark qSongBookmark = QSongBookmark.songBookmark;
        QSongCowriter qSongCowriter = QSongCowriter.songCowriter;
        List<SongResponse> result = null;

        try {
            result = queryFactory.select(Projections.fields(SongResponse.class,
                            qSong.songId, qSong.originalSongId, qSong.songTitle, qSong.songSubTitle,
                            qSong.regDt, qSong.regUsr, qSong.avatarFileUrl, qSong.avatarImgUseYn,
                            qSong.songStatusCd, qSong.userDefineVersion, qSong.songVersionCd, qSong.songOwner,
                            ExpressionUtils.as(
                                    JPAExpressions.select(qSongBookmark.bookmarkYn)
                                            .from(qSongBookmark)
                                            .where(qSongBookmark.songId.eq(qSong.songId))
                                            .orderBy(qSongBookmark.modDt.desc())
                                            .limit(1)
                                    ,"bookmarkYn"
                            )
                    )
            ).from(qSong).innerJoin(qProjectSong)
            .on(qSong.songId.eq(qProjectSong.songId))
            .where(qProjectSong.projId.eq(projId)
                    .and(qSong.songId.in(JPAExpressions
                                    .select(qSongCowriter.songId)
                                    .from(qSongCowriter)
                                    .where(qSongCowriter.profileId.eq(profileId))
                            )
                    )
                )
            .offset(offset)
            .limit(limit)
            .fetch();

        } catch (Exception e) {
            logger.error("##### select List Error ##### ", e.getMessage());
        }

        return result;
    }

    @Override
    public List<SongResponse> selectSongVersionList(Long projId, Long originalSongId) {
        QSong qSong = QSong.song;
        QProjectSong qProjectSong = QProjectSong.projectSong;
        List<SongResponse> result = null;
        try {
            result = queryFactory.select(Projections.fields(SongResponse.class,
                                    qSong.songId, qSong.originalSongId
                                    ,qSong.songVersionCd, qSong.userDefineVersion
                            )
                    ).from(qSong).innerJoin(qProjectSong)
                    .on(qSong.songId.eq(qProjectSong.songId))
                    .where(qProjectSong.projId.eq(projId)
                            .and(qSong.originalSongId.eq(originalSongId))
                        )
                    .fetch();

        } catch (Exception e) {
            logger.error("##### select List Error ##### ", e.getMessage());
        }
        return result;
    }

    @Override
    public Long songRepositoryDynamicUpdate(SongBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSong);

        // title
        if(!StringUtils.isEmpty(entity.getSongTitle())){
            jpaUpdateClause.set(qSong.songTitle, entity.getSongTitle());
        }

        // sub title
        if(!StringUtils.isEmpty(entity.getSongSubTitle())){
            jpaUpdateClause.set(qSong.songSubTitle, entity.getSongSubTitle());
        }

        //description
        if(!StringUtils.isEmpty(entity.getDescription())){
            jpaUpdateClause.set(qSong.description, entity.getDescription());
        }

        // avatar Yn
        if(!StringUtils.isEmpty(entity.getAvatarImgUseYn())){
            jpaUpdateClause.set(qSong.avatarImgUseYn, entity.getAvatarImgUseYn());

            if(StringUtils.equals(entity.getAvatarImgUseYn(), CommonCodeConst.YN.Y)){
                jpaUpdateClause.set(qSong.avatarFileUrl, entity.getAvatarFileUrl());
            }
        }

        if(entity.getModUsr() != null) {
            jpaUpdateClause.set(qSong.modDt, LocalDateTime.now());
            jpaUpdateClause.set(qSong.modUsr, entity.getModUsr());
        }


//        jpaUpdateClause.where(qSong.songId.eq(entity.getSongId()));
        jpaUpdateClause.where(qSong.originalSongId.eq(entity.getOriginalSongId()));
//        jpaUpdateClause.where(qSong.songOwner.eq(entity.getSongOwner()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }


    @Override
    public Long songDetailUpdate(SongBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSong);

        //description
        if(!StringUtils.isEmpty(entity.getSongAvailYn())){
            jpaUpdateClause.set(qSong.songAvailYn, entity.getSongAvailYn());
        }

        if(entity.getModUsr() != null) {
            jpaUpdateClause.set(qSong.modDt, LocalDateTime.now());
            jpaUpdateClause.set(qSong.modUsr, entity.getModUsr());
        }

        jpaUpdateClause.where(qSong.songId.eq(entity.getSongId()));

        Long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
