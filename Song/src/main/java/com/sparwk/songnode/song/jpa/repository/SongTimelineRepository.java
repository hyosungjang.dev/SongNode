package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongTimeline;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongTimelineRepository extends JpaRepository<SongTimeline, Long> {

    List<SongTimeline> findSongTimelinesBySongIdOrderByRegDtDesc(Long songId);
}
