package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongFileBaseDTO {
    private Long songId;
    private Long songFileSeq;
    private String songFileName;
    private String songFilePath;
    private Long songFileSize;
    private String songFileComt;
    private Long bpm;
    private String containSampleYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
