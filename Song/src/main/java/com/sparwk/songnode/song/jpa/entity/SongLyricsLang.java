package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongLyricsLangId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_lyrics_lang")
@IdClass(SongLyricsLangId.class)
public class SongLyricsLang {

    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "lang_cd", nullable = true)
    private String langCd;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    SongLyricsLang(
            Long songId,
            String langCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.langCd = langCd;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }

}
