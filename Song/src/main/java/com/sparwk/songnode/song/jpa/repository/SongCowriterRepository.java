package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.entity.SongCowriter;
import com.sparwk.songnode.song.jpa.repository.dsl.ProjectSongCowriterRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCowriterRepository extends JpaRepository<SongCowriter, Long>, ProjectSongCowriterRepositoryDynamic {

    // find song cowriter fetch multi
    List<SongCowriter> findSongCowritersBySongId(Long songId);
    // find song cowriter fetch one
    SongCowriter findSongCowriterBySongIdAndProfileId(Long songId, Long profileId);
    // delete
    Long deleteSongCowriterBySongIdAndProfileId(Long songId, Long profileId);

    Long countBySongIdAndAcceptYn(Long songId, String acceptYn);
}