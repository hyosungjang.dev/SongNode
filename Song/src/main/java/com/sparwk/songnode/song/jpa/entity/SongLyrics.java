package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.entity.id.SongLyricsId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_lyrics")
@IdClass(SongLyricsId.class)
public class SongLyrics extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "song_file_seq", nullable = true)
    private Long songFileSeq;
    @Column(name = "lyrics", nullable = true)
    private String lyrics;
    @Column(name = "lyrics_comt", nullable = true)
    private String lyricsComt;
    @Column(name = "explicit_content_yn", nullable = true)
    private String explicitContentYn = CommonCodeConst.YN.N;
    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;
    @Builder
    SongLyrics(
            Long songId,
            Long songFileSeq,
            String lyrics,
            String lyricsComt,
            String explicitContentYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
        this.explicitContentYn = explicitContentYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }

}
