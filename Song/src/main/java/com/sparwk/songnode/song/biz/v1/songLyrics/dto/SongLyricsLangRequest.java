package com.sparwk.songnode.song.biz.v1.songLyrics.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongLyricsLangRequest {

    @Schema(description = "작업자 profileId")
    private Long profileId;
    @Schema(description = "송 아이디")
    private Long songId;
    @Schema(description = "가사 언어 리스트")
    private String langCdList;
}
