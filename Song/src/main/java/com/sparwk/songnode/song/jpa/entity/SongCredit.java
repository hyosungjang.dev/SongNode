package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongCreditId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_credit")
@IdClass(SongCreditId.class)
public class SongCredit{

    @Id
    @GeneratedValue(generator = "tb_song_credit_seq")
    @Column(name = "song_credit_seq", nullable = true)
    private Long songCreditSeq;

    @Id
    @Column(name = "song_id")
    private Long songId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "delete_yn", nullable = true)
    private String deleteYn;

    @Column(name = "ipn_number", nullable = true)
    private String ipnNumber;
    @Column(name = "nro_profile_id", nullable = true)
    private Long nroProfileId;
    @Column(name = "nro_number", nullable = true)
    private String nroNumber;


    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    SongCredit(
            Long songId,
            Long profileId,
            String deleteYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            Long songCreditSeq,
            String ipnNumber,
            Long nroProfileId,
            String nroNumber
    ) {
        this.songId = songId;
        this.profileId = profileId;
        this.deleteYn = deleteYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
        this.songCreditSeq          =       songCreditSeq;
        this.ipnNumber              =       ipnNumber;
        this.nroProfileId           =       nroProfileId;
        this.nroNumber              =       nroNumber;
    }

}
