package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongMetadataId implements Serializable {

    private Long songId;
    private String attrTypeCd;
    private String attrDtlCd;

}
