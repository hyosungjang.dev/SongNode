package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.dto.SongMetadataBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongMetadataRepository extends JpaRepository<SongMetadata, Long>{
    List<SongMetadata> findBySongId(Long songId);
    List<SongMetadata> findSongMetadataBySongId(Long songId);

    Long deleteSongMetadataBySongId(Long songId);
}
