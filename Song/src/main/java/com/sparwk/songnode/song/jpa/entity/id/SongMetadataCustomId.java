package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongMetadataCustomId implements Serializable {
    private Long songId;
    private Long metadataSeq;
}
