package com.sparwk.songnode.song.biz.v1.songLyrics;

import com.sparwk.songnode.song.biz.v1.songFile.SongFileDomain;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsLangRequest;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = Environment.API_VERSION + "/lyrics")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = "Song Server SongLyrics")
public class SongLyricsController {

    private final SongLyricsDomain songLyricsDomain;
    private final Logger logger = LoggerFactory.getLogger(SongFileDomain.class);

    @ApiOperation(
            value = "Song lyrics modify"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/modify")
    public Response<Void> updateSongLyrics(
            HttpServletRequest req,
            @RequestBody SongLyricsDto songLyricsDto
    ) {
        logger.info("##### updateSongLyrics Modify #####");
        return songLyricsDomain.modifySongLyrics(songLyricsDto, req);
    }


    @ApiOperation(
            value = "Song lyrics lang add & modify"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/save/lang")
    public Response<Void> saveSongLyricsLang(
            @RequestBody SongLyricsLangRequest songLyricsLangRequest
    ) {
        logger.info("##### saveSongLyricsLang Add, Modify #####");
        return songLyricsDomain.saveSongLyricsLang(songLyricsLangRequest);
    }

}
