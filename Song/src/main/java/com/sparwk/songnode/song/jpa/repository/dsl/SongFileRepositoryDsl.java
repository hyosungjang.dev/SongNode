package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;

public interface SongFileRepositoryDsl {
    Long songFileRepositoryDynamicUpdate(SongFileDto dto);
}
