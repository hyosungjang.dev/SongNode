package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataCustomDTO {
    private Long songId;
    private Long metadataSeq;
    private String metadataName;
    private String metadataValue;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
