package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.jpa.dto.SongCreditBaseDTO;

public interface SongCreditRepositoryDsl {

    Long updateCreditInfo(SongCreditBaseDTO songCreditBaseDTO);
}
