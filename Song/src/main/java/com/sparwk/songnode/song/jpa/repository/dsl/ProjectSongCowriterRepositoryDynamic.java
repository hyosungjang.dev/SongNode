package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.jpa.dto.SongCowriterBaseDTO;

public interface ProjectSongCowriterRepositoryDynamic {

    Long updateRateShareSplitSheet(SongCowriterBaseDTO songCowriterBaseDTO, String type);

    Long updateAcceptSplitSheet(SongCowriterBaseDTO songCowriterBaseDTO);

    Long updateCowriterInfo(SongCowriterBaseDTO songCowriterBaseDTO);
}
