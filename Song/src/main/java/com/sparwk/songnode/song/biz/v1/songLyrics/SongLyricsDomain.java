package com.sparwk.songnode.song.biz.v1.songLyrics;

import com.sparwk.songnode.song.biz.v1.songFile.SongFileService;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsLangRequest;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SongLyricsDomain {

    private final SongFileService songFileService;
    private final SongLyricsService songLyricsService;

    private Logger logger = LoggerFactory.getLogger(SongLyricsDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;

    public Response<List<SongFileDto>> selectSongFile(Long songId, HttpServletRequest req) {
        logger.info("###### selectSongFile ######");
        Response<List<SongFileDto>> res = new Response<>();

        List<SongFileDto> result = songFileService.songFileSelectService(songId);

        res.setResultCd(ResultCodeConst.SUCCESS.name());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * Song Lyrics modify
     * @param songLyricsDto
     * @param req
     * @return
     */
    public Response<Void> modifySongLyrics(SongLyricsDto songLyricsDto, HttpServletRequest req) {
        logger.info("#### modifySongLyrics ####");
        Response<Void> res = new Response<>();
        songLyricsService.songLyricsDynamicUpdate(songLyricsDto);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * song lang add and modify
     * @param songLyricsLangRequest
     * @return
     */
    public Response<Void> saveSongLyricsLang(SongLyricsLangRequest songLyricsLangRequest) {
        Response<Void> res = new Response<>();
        songLyricsService.deleteSongLyricsLangAll(songLyricsLangRequest.getSongId());
        songLyricsService.saveSongLyricsLang(songLyricsLangRequest);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
