package com.sparwk.songnode.song.biz.v1.SongCredit.dto;


import com.sparwk.songnode.song.jpa.dto.SongCreditBaseDTO;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCreditDetailResponse {

    private SongCreditBaseDTO songCreditResponse;
    private String roleCdList;

}
