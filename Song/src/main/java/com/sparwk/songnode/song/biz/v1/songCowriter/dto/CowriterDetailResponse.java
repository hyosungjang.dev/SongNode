package com.sparwk.songnode.song.biz.v1.songCowriter.dto;


import com.sparwk.songnode.song.jpa.dto.SongCowriterBaseDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CowriterDetailResponse {

    private SongCowriterBaseDTO songCowriterResponse;
    private String roleCdList;
    private String spProfileId;

}
