package com.sparwk.songnode.song.biz.v1.songMetadata;

import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataCustomRequest;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataRequest;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataResponse;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = Environment.API_VERSION + "/metadata")
@CrossOrigin("*")
@Api(tags = "Song Server SongMetadata")
@RequiredArgsConstructor
public class SongMetadataController {


    private final SongMetadataDomain songMetadataDomain;
    private final Logger logger = LoggerFactory.getLogger(SongMetadataController.class);

    @ApiOperation(
            value = "Song metadata"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "songId", value = "songId", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/list/{songId}")
    public Response<SongMetadataResponse> selectSongMetadata(
            HttpServletRequest req,
            @PathVariable(name = "songId") Long  songId
    ) {
        logger.info("#### selectSongDetail ####");
        return songMetadataDomain.getSongMetadata(songId, req);
    }

    @Operation(
            description = "메타데이터 추가",
            parameters = {
                    @Parameter(name = "songMetadataDto", in = ParameterIn.QUERY, required = true, description = "송 정보 업데이트")
            }
    )
    @ApiOperation(
            value = "Song metadata save"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/save")
    public Response<Void> updateSongMetadataCustom (
            HttpServletRequest req,
            @RequestBody SongMetadataRequest songMetadataRequest
    ) {
        logger.info("#### selectSongDetail ####");
        return songMetadataDomain.modifySongMetadata(songMetadataRequest ,req);
    }


    @Operation(
            description = "사용자 메타데이터 추가",
            parameters = {
                    @Parameter(name = "songMetadataCustomDto", in = ParameterIn.QUERY, required = true, description = "송 정보 업데이트")
            }
    )
    @ApiOperation(
            value = "Song metadata custom save"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/save/custom")
    public Response<Void> insertSongMetadataCustom (
            HttpServletRequest req,
            @RequestBody SongMetadataCustomRequest songMetadataCustomRequest
    ) {
        logger.info("#### selectSongDetail ####");
        return songMetadataDomain.saveSongMetadataCustom(songMetadataCustomRequest);
    }



//    @Operation(
//            description = "사용자 메타데이터 삭제",
//            parameters = {
//                    @Parameter(name = "songMetadataCustomDto", in = ParameterIn.QUERY, required = true, description = "송 정보 업데이트")
//            }
//    )
//    @ApiOperation(
//            value = "Song metadata custom Delete"
//    )
//    @ApiResponses({
//            @ApiResponse(code=0000, message = "성공"),
//    })
//    @DeleteMapping(path = "/delete/custom")
//    public Response<Void> deleteSongMetadataCustom (
//            HttpServletRequest req,
//            @RequestBody SongMetadataCustomDto songMetadataCustomDto
//    ) {
//        logger.info("#### deleteSongMetadataCustom ####");
//        return songMetadataDomain.deleteSongMetadataCustom(songMetadataCustomDto, req);
//    }

}
