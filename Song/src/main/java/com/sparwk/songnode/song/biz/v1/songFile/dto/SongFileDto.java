package com.sparwk.songnode.song.biz.v1.songFile.dto;

import com.sparwk.songnode.song.biz.v1.common.dto.SongCommonParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongFileDto {

    @Schema(description = "songCommonDto")
    private SongCommonParam songCommonParam;

    @Schema(description = "노래 아이디")
    private Long songId;
    @Schema(description = "노래 가사 변경 시퀀스")
    private Long songFileSeq;
    @Schema(description = "노래 등록 파일 명")
    private String songFileName;
    @Schema(description = "노래 등록 파일 경로")
    private String songFilePath;
    @Schema(description = "노래 파일 사이즈")
    private Long songFileSize;
    @Schema(description = "노래에 대한 멘트")
    private String songFileComt;
    @Schema(description = "BPM값")
    private Long bpm;
    @Schema(description = "샘플Yn")
    private String containSampleYn;
}
