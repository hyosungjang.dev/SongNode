
package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongCowriterRoleId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongCowriterRoleId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_cowriter_role")
public class SongCowriterRole {


    @Id
    @Column(name = "song_cowriter_seq", nullable = true)
    private Long songCowriterSeq;
    @Id
    @Column(name = "role_cd", nullable = true)
    private String roleCd;

    @Builder
    SongCowriterRole(
            Long songCowriterSeq,
            String roleCd
    ) {
        this.songCowriterSeq = songCowriterSeq;
        this.roleCd = roleCd;
    }

}
