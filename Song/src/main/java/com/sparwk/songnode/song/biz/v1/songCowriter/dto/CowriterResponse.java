package com.sparwk.songnode.song.biz.v1.songCowriter.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CowriterResponse {

    @Schema(description = "Song profileId")
    private Long profileId;
    @Schema(description = "Song 아이디")
    private Long songId;
    @Schema(description = "지분율")
    private Double rateShare;
    @Schema(description = "수락여부")
    private String acceptYn;

    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

    private Long songCowriterSeq;
    private String personIdType;
    private String personIdNumber;
    private Long opProfileId;
    private Long proProfileId;
    private LocalDateTime acceptDt;
    private String copyrightControlYn;
    private String rateShareComt;

}
