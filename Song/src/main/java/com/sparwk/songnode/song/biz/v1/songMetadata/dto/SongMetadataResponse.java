package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataResponse {
    @Schema(description = "노래 메타데이타")
    private List<SongMetadataDto> songMetadataList;
    @Schema(description = "노래 사용자 메타데이터")
    private List<SongMetadataCustomDto> songMetadataCustomList;
}
