package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongCowriterSp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCowriterSpRepository extends JpaRepository<SongCowriterSp, Long>{
    List<SongCowriterSp> findSongCowriterSpsBySongCowriterSeq(Long songCowriterSeq);

    Long deleteSongCowriterSpsBySongCowriterSeq(Long songCowriterSeq);
}