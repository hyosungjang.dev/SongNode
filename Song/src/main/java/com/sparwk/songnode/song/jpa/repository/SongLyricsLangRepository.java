package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongLyricsLang;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongLyricsLangRepository extends JpaRepository<SongLyricsLang, Long> {

    List<SongLyricsLang> findSongLyricsLangsBySongId(Long songId);
    Long deleteSongLyricsLangsBySongId(Long songId);
}
