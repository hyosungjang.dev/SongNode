package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongBookmarkId implements Serializable {
    private Long profileId;
    private Long songId;
}
