package com.sparwk.songnode.song.biz.v1.songFile;

import com.sparwk.songnode.song.biz.v1.song.SongService;
import com.sparwk.songnode.song.biz.v1.songLyrics.SongLyricsService;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileAndLyricsResponse;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SongFileDomain {

    private final SongService songService;
    private final SongFileService songFileService;
    private final SongLyricsService songLyricsService;

    private Logger logger = LoggerFactory.getLogger(SongFileDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;

    /**
     * Song file and lyrics
     * @param songId
     * @param req
     * @return
     */
    public Response<SongFileAndLyricsResponse> selectSongFile(Long songId, HttpServletRequest req) {
        Response<SongFileAndLyricsResponse> res = new Response<>();

        // ASIS: just one song file
        // TODO: multi file for Song.
        SongFileAndLyricsResponse result = new SongFileAndLyricsResponse();

        List<SongFileDto> songFileBaseDto = songFileService.songFileSelectService(songId);
        SongLyricsDto songLyricsDto = songLyricsService.songLyricsSelectService(songId, songFileBaseDto.get(0).getSongFileSeq());
        // response setting
        if(songFileBaseDto.size() > 0){
            result.setSongFileDto(songFileBaseDto.get(0));
        }
        if(songLyricsDto != null){
            result.setSongLyricsDto(songLyricsDto);
        }
        res.setResultCd(ResultCodeConst.SUCCESS.name());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * song file update method
     * @param songFileDto
     * @param req
     * @return
     */
    public Response<Void> modifySongFile(SongFileDto songFileDto, HttpServletRequest req) {
        logger.info("#### modifySongFile ####");
        Response<Void> res = new Response<>();
        songFileService.songFileDynamicUpdate(songFileDto);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
