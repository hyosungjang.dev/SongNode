
package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongCowriterSpId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongCowriterSpId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_cowriter_sp")
public class SongCowriterSp {


    @Id
    @Column(name = "song_cowriter_seq", nullable = true)
    private Long songCowriterSeq;
    @Id
    @Column(name = "sp_profile_id", nullable = true)
    private Long spProfileId;

    @Builder
    SongCowriterSp(
            Long songCowriterSeq,
            Long spProfileId
    ) {
        this.songCowriterSeq = songCowriterSeq;
        this.spProfileId = spProfileId;
    }

}
