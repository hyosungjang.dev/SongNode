package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.biz.v1.song.dto.SongResponse;
import com.sparwk.songnode.song.jpa.dto.SongBaseDTO;

import java.util.List;

public interface SongRepositoryDsl {

    Long songRepositoryDynamicUpdate(SongBaseDTO dto);
    Long songDetailUpdate(SongBaseDTO dto);

    List<SongResponse> selectSongList(Long projId, Long profileId, int offset, int limit);

    List<SongResponse> selectSongVersionList(Long projId, Long originalSongId);
}
