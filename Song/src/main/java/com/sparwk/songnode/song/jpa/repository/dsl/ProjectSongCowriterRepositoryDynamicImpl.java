package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongCowriterBaseDTO;
import com.sparwk.songnode.song.jpa.entity.QSongCowriter;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class ProjectSongCowriterRepositoryDynamicImpl implements ProjectSongCowriterRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProjectSongCowriterRepositoryDynamicImpl.class);

    private ProjectSongCowriterRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long updateRateShareSplitSheet(SongCowriterBaseDTO songCowriterBaseDTO, String type) {
        logger.info("#### JPA UPDATE:: updateRateShareSplitSheet IN <<<<< {} ",songCowriterBaseDTO);

        QSongCowriter qSongCowriter = QSongCowriter.songCowriter;
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongCowriter);
        jpaUpdateClause.set(qSongCowriter.rateShare, songCowriterBaseDTO.getRateShare());
        jpaUpdateClause.set(qSongCowriter.modDt, songCowriterBaseDTO.getModDt());
        jpaUpdateClause.set(qSongCowriter.modUsr, songCowriterBaseDTO.getModUsr());
        if(!StringUtils.equals(type, CommonCodeConst.MODIFY.RESET)) {
            jpaUpdateClause.set(qSongCowriter.acceptYn, songCowriterBaseDTO.getAcceptYn());
            jpaUpdateClause.set(qSongCowriter.acceptDt, songCowriterBaseDTO.getAcceptDt());
            jpaUpdateClause.where(qSongCowriter.profileId.eq(songCowriterBaseDTO.getProfileId()));
        }
        jpaUpdateClause.where(qSongCowriter.songId.eq(songCowriterBaseDTO.getSongId()));

        Long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }


    /**
     * Accept Yn for profile Id
     * @param songCowriterBaseDTO
     * @return
     */
    @Override
    public Long updateAcceptSplitSheet(SongCowriterBaseDTO songCowriterBaseDTO) {
        logger.info("#### JPA UPDATE:: updateAcceptSplitSheet IN <<<<< {} ",songCowriterBaseDTO);
        QSongCowriter qSongCowriter = QSongCowriter.songCowriter;
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongCowriter);

        jpaUpdateClause.set(qSongCowriter.acceptYn, songCowriterBaseDTO.getAcceptYn());
        jpaUpdateClause.set(qSongCowriter.acceptDt, songCowriterBaseDTO.getAcceptDt());
        jpaUpdateClause.set(qSongCowriter.modDt, songCowriterBaseDTO.getModDt());
        jpaUpdateClause.set(qSongCowriter.modUsr, songCowriterBaseDTO.getModUsr());

        // 만약 거절 했다면
        if(StringUtils.equals(songCowriterBaseDTO.getAcceptYn(), CommonCodeConst.YN.N)){
            jpaUpdateClause.set(qSongCowriter.rateShareComt, songCowriterBaseDTO.getRateShareComt());
        }

        jpaUpdateClause.where(qSongCowriter.profileId.eq(songCowriterBaseDTO.getProfileId()));
        jpaUpdateClause.where(qSongCowriter.songId.eq(songCowriterBaseDTO.getSongId()));

        Long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }

    @Override
    public Long updateCowriterInfo(SongCowriterBaseDTO songCowriterBaseDTO) {
        logger.info("#### JPA UPDATE:: updateCowriterInfo IN <<<<< {} ",songCowriterBaseDTO);
        QSongCowriter qSongCowriter = QSongCowriter.songCowriter;
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongCowriter);

        // copy right Control
        if(!StringUtils.isEmpty(songCowriterBaseDTO.getCopyrightControlYn())) {
            jpaUpdateClause.set(qSongCowriter.copyrightControlYn, songCowriterBaseDTO.getCopyrightControlYn());
        }

        // personId
        if(!StringUtils.isEmpty(songCowriterBaseDTO.getPersonIdType())) {
            jpaUpdateClause.set(qSongCowriter.personIdType, songCowriterBaseDTO.getPersonIdType());
            jpaUpdateClause.set(qSongCowriter.personIdNumber, songCowriterBaseDTO.getPersonIdNumber());
        }

        // original publisher
        if(songCowriterBaseDTO.getOpProfileId() != null) {
            jpaUpdateClause.set(qSongCowriter.opProfileId, songCowriterBaseDTO.getOpProfileId());
        }

        // PRO
        if(songCowriterBaseDTO.getProfileId() != null) {
            jpaUpdateClause.set(qSongCowriter.proProfileId, songCowriterBaseDTO.getProProfileId());
        }

        jpaUpdateClause.set(qSongCowriter.modUsr, songCowriterBaseDTO.getModUsr());
        jpaUpdateClause.set(qSongCowriter.modDt, LocalDateTime.now());

        jpaUpdateClause.where(qSongCowriter.songId.eq(songCowriterBaseDTO.getSongId()));
        jpaUpdateClause.where(qSongCowriter.profileId.eq(songCowriterBaseDTO.getProfileId()));

        Long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
