package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MainSubGenrDto {

    @Schema(description = "장르 코드")
    private String genresCd;
    @Schema(description = "장르 리스트")
    private String genresList;
    @Schema(description = "서브장르 코드")
    private String subgenresCd;
    @Schema(description = "서브장르 리스트")
    private String subgenresList;
    @Schema(description = "서브장르 노래 속성 포맷")
    private String subAttrFormat;

}
