package com.sparwk.songnode.song.biz.v1.songCowriter.dto.split;


import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.SplitSheetCommonRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SplitSheetRequest {

    @Schema(name = "ownerProfileId")
    private Long ownerProfileId;
    @Schema(name = "songId")
    private Long songId;
    @Schema(name = "cowriterList")
    private List<SplitSheetCommonRequest> cowriterList;

}
