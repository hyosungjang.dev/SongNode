package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.biz.v1.projectSong.dto.ProjectSongCountResponse;
import com.sparwk.songnode.song.jpa.entity.ProjectSong;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectSongRepository extends JpaRepository<ProjectSong, Long> {


        @Query(nativeQuery = true, value = "select\n" +
                "    A.proj_id AS projId,\n" +
                "    A.profile_id as profileId,\n" +
                "    COALESCE(C.count ,0) AS songCnt\n" +
                "from tb_project_memb A left outer join\n" +
                "     (select\n" +
                "          profile_id, count(song_id)\n" +
                "      from (\n" +
                "               select song_id,\n" +
                "                      profile_id\n" +
                "               from (\n" +
                "                        select A.song_id, profile_id\n" +
                "                        from tb_song_cowriter A inner join tb_project_song B\n" +
                "                        on A.song_id = B.song_id\n" +
                "                        where profile_id in (select profile_id\n" +
                "                                             from tb_project_memb\n" +
                "                                             where proj_id = :projId )\n" +
                "                        and B.proj_id= :projId \n" +
                "                        union all\n" +
                "                        select A.song_id, profile_id\n" +
                "                        from tb_song_credit A inner join tb_project_song B\n" +
                "                        on A.song_id = B.song_id\n" +
                "                        where profile_id in (select profile_id\n" +
                "                                             from tb_project_memb\n" +
                "                                             where proj_id = :projId )\n" +
                "                        and B.proj_id = :projId \n" +
                "                    ) A\n" +
                "               group by song_id, profile_id\n" +
                "           ) B\n" +
                "      GROUP BY profile_id) C\n" +
                "     on A.profile_id = C.profile_id\n" +
                "where A.proj_id = :projId \n" +
                "order by reg_dt asc")
        List<ProjectSongCountResponse> findProjIdAndProfileIdAndSongCntByProjId(
                @Param("projId") Long projId);

        ProjectSong findProjectSongByProjIdAndSongId(Long projId, Long songId);

        Long deleteProjectSongByProjIdAndSongId(Long projId, Long songId);
}