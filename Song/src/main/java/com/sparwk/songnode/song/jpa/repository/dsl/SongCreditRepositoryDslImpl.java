package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.song.jpa.dto.SongCreditBaseDTO;
import com.sparwk.songnode.song.jpa.entity.QSongCredit;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class SongCreditRepositoryDslImpl implements SongCreditRepositoryDsl {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(SongCreditRepositoryDslImpl.class);

    private SongCreditRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long updateCreditInfo(SongCreditBaseDTO songCreditBaseDTO) {
        logger.info("#### JPA UPDATE:: updateCreditInfo IN <<<<< {} ",songCreditBaseDTO);
        QSongCredit qSongCredit = QSongCredit.songCredit;
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongCredit);

        // copy right Control
        if(!StringUtils.isEmpty(songCreditBaseDTO.getIpnNumber())) {
            jpaUpdateClause.set(qSongCredit.ipnNumber, songCreditBaseDTO.getIpnNumber());
        }

        // personId
        if(!StringUtils.isEmpty(songCreditBaseDTO.getNroNumber())) {
            jpaUpdateClause.set(qSongCredit.nroNumber, songCreditBaseDTO.getNroNumber());
        }

        // original publisher
        if(songCreditBaseDTO.getNroProfileId() != null) {
            jpaUpdateClause.set(qSongCredit.nroProfileId, songCreditBaseDTO.getNroProfileId());
        }

        jpaUpdateClause.set(qSongCredit.modUsr, songCreditBaseDTO.getModUsr());
        jpaUpdateClause.set(qSongCredit.modDt, LocalDateTime.now());

        jpaUpdateClause.where(qSongCredit.songId.eq(songCreditBaseDTO.getSongId()));
        jpaUpdateClause.where(qSongCredit.profileId.eq(songCreditBaseDTO.getProfileId()));

        Long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
