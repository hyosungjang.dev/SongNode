package com.sparwk.songnode.song.biz.v1.songCowriter.dto.split;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AcceptSplitSheetRequest {

    @Schema(name = "profileId")
    private Long profileId;
    @Schema(name = "songId")
    private Long songId;
    @Schema(name = "acceptYn")
    private String acceptYn;
    @Schema(name = "rateShareComt")
    private String rateShareComt;

}
