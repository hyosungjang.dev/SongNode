package com.sparwk.songnode.song.biz.v1.song;

import com.sparwk.songnode.song.biz.v1.song.dto.*;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.MainSubGenrDto;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.SongServiceBaseRepository;
import com.sparwk.songnode.song.jpa.dto.SongBaseDTO;
import com.sparwk.songnode.song.jpa.dto.SongMetadataBaseDTO;
import com.sparwk.songnode.song.jpa.entity.ProjectSong;
import com.sparwk.songnode.song.jpa.entity.Song;
import com.sparwk.songnode.song.jpa.entity.SongBookmark;
import com.sparwk.songnode.song.jpa.repository.*;
import com.sparwk.songnode.song.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongService {

    // song
    private final SongRepository songRepository;
    private final SongServiceBaseRepository songServiceBaseRepository;
    // song - project 연계
    final private SongMetadataRepository songMetadataRepository;

    private final SongCowriterRepository projectSongCowriterRepository;
    private final ProjectSongRepository projectSongRepository;

    private final SongBookmarkRepository songBookmarkRepository;

    private final HttpServletRequest request;

    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(SongService.class);

    /**
     * create song valid
     * case:
     *  song basic parameter is null
     *  projectId is null
     * @param dto
     * @param req
     * @return
     */
    public String createSongValid(SongRequest dto, HttpServletRequest req) {
        if(dto.getSongDto() == null) {
            return ResultCodeConst.NOT_REQUIRE_SONG_PARAMETERS.getCode();
        }
        return ResultCodeConst.SUCCESS.getCode();
    }

    /**
     * SONG SEQ INIT
     *  file SEQ를 위한 초기화
     * @return
     */
    public Long initSongSeq() {
        return songRepository.newSongSeq();
    }

    @Transactional
    public SongBaseDTO projectSongSaveService(SongRequest dto) {
        logger.info("##### projectSongSaveService ######  >>>> {} ", dto);

        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);

        SongBaseDTO result = null;
        try {
//            long songId = songRepository.newSongSeq();
            // data 초기 setting
            dto.getSongDto().setSongAvailYn(CommonCodeConst.YN.N);
            dto.getSongDto().setSongVersionCd(CommonCodeConst.SONG_VERSION_CD.ORIGIAL_VERSION);
            dto.getSongDto().setSongStatusCd(CommonCodeConst.SONG_STATUS_CD.AVAILABLE);
            dto.getSongDto().setSongStatusModDt(LocalDateTime.now());
            SongBaseDTO songBaseDTO = modelMapper.map(dto.getSongDto(), SongBaseDTO.class);
            songBaseDTO.setRegUsr(userInfoDTO.getLastUseProfileId());
            songBaseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
            songBaseDTO.setModDt(LocalDateTime.now());
            songBaseDTO.setRegDt(LocalDateTime.now());

            Song entity = modelMapper.map(songBaseDTO, Song.class);
            songRepository.save(entity);
            result = modelMapper.map(entity, SongBaseDTO.class);

        } catch (Exception e) {
            logger.error("##### Error Save Song >>>>>> {} ", e.getMessage());
        }
        return result;
    }

    @Transactional
    public void songSaveGenre(MainSubGenrDto dto, Long songId) {
        logger.info("### Service : songSaveGenre >>>dto : {} >>>> song ID: {}", dto, songId);
        try {
            String[] genr = dto.getGenresList().split(",");
            String[] subGenr = dto.getSubgenresList().split(",");
            // 장르추가
            for (String e : genr) {
                SongMetadataBaseDTO songMeta = SongMetadataBaseDTO.builder()
                        .songId(songId)
                        .attrTypeCd(dto.getGenresCd())
                        .attrDtlCd(e)
                        .build();
                songServiceBaseRepository.SongMetadataSaveService(songMeta);
            }

            // 서브장르추가
            for (String e : subGenr) {
                SongMetadataBaseDTO songMeta = SongMetadataBaseDTO
                        .builder()
                        .songId(songId)
                        .attrTypeCd(dto.getGenresCd())
                        .attrDtlCd(e)
                        .build();
                songServiceBaseRepository.SongMetadataSaveService(songMeta);
            }
        } catch (Exception e) {
            logger.error("#### JPA SAVE ERROR: songSaveGenre #### {}", e.getMessage());
        }
    }



    @Transactional
    public void saveSongProject(Long projId, Long songId) {
        logger.info("##### saveSongProject >>>>projID  {}, songId {}",projId, songId);
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        try {
            ProjectSong projectSong = ProjectSong.builder()
                    .projId(projId)
                    .songId(songId)
                    .regUsr(userInfoDTO.getLastUseProfileId())
                    .regDt(LocalDateTime.now())
                    .modUsr(userInfoDTO.getLastUseProfileId())
                    .modDt(LocalDateTime.now())
                    .build();
            projectSongRepository.save(projectSong);
        } catch (Exception e){
            logger.error("####  saveSongProject ERROR #### {}", e.getMessage());
        }
    }

    @Transactional
    public Long modifySongDetail(SongModifyRequest songModifyRequest) {
        logger.info("#### modifySongDetail >>>>> {}", songModifyRequest);

        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        SongBaseDTO songBaseDTO = modelMapper.map(songModifyRequest, SongBaseDTO.class);
        songBaseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
        Long result = songRepository.songRepositoryDynamicUpdate(songBaseDTO);
        return result;
    }

    /**
     * song List
     * @param projId
     * @param offset
     * @param limit
     * @return
     */
    public List<SongResponse> selectSongList(Long projId,Long profileId, int offset, int limit) {
        logger.info("### Service : selectSongList >>>projId : {} >>>>:: profileId : {}>>>> offset : {}, limit : {}", projId, profileId ,offset,limit);
        List<SongResponse> list = null;
        try{
            list = songRepository.selectSongList(projId,profileId,offset, limit);
        } catch (Exception e){
            logger.error("##### SELECT JPA ERROR #####");
        }
        return list;
    }

    /**
     *
     * @param projId
     * @param originalSongId
     * @return
     */
    public List<SongResponse> selectSongVersionList(Long projId, Long originalSongId) {
        logger.info(" ### Service : selectSongVersionList >>> proj Id :{} ,>>> originalSongId : {}", projId, originalSongId);
        return songRepository.selectSongVersionList(projId, originalSongId);
    }



    /**
     * metadata List
     * @param songId
     * @return
     */
    public List<SongMetadataBaseDTO> selectSongMetadata(Long songId) {
        logger.info(" ### Service : selectSongMetadata >>> songId Id :{}", songId);
        return songMetadataRepository.findSongMetadataBySongId(songId).stream()
                .map(e -> modelMapper.map(e, SongMetadataBaseDTO.class)).collect(Collectors.toList());
    }


    /**
     * song Detail
     * @param projId
     * @param songId
     * @return
     */
    public SongDto selectSongDetailInfo(Long projId, Long songId){
        logger.info("#### song detail info ####");
        SongDto result = null;
        try {

            if(projectSongRepository.findProjectSongByProjIdAndSongId(projId, songId) == null) {
                return null;
            }
            Song song = songRepository.findSongBySongId(songId);
            if(song == null){
                return new SongDto();
            }
            result = modelMapper.map(song, SongDto.class);

        } catch (Exception e) {
            logger.error("###### GET SONG DETAIL ERROR ##### ");
        }

        return result;
    }

    /**
     * song copy
     * @param vo
     * @return
     */
    @Transactional
    public void songCopy(SongVersionRequest vo){
        logger.info("#### song createSongCopy ####");

        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        SongDto song = modelMapper.map(songRepository.findSongBySongId(vo.getSongId())
                        ,SongDto.class);

        song.setModDt(LocalDateTime.now());
        song.setModUsr(userInfoDTO.getLastUseProfileId());

        if(song != null) {
            // new song copy
            song.setSongId(vo.getNewSongId());
            song.setSongVersionCd(vo.getSongVersionCd());
            song.setSongAvailYn(CommonCodeConst.YN.N);
            if(StringUtils.equals(vo.getSongVersionCd(), CommonCodeConst.SONG_VERSION_CD.USER_DEFINED)){
                song.setUserDefineVersion(vo.getUserDefineVersion());
            }
            songRepository.save(modelMapper.map(song, Song.class));
        }
    }


    /**
     * song bookmark
     * @param req
     */
    @Transactional
    public void songBookmark(SongBookmarkRequest req) {
        logger.info("#### ENTER songBookmark ####");
        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        try {
            SongBookmark entity = SongBookmark.builder()
                    .songId(req.getSongCommonParam().getSongId())
                    .profileId(userInfoDTO.getLastUseProfileId())
                    .bookmarkYn(req.getBookmarkYn().toUpperCase())
                    .modDt(LocalDateTime.now())
                    .regDt(LocalDateTime.now())
                    .modUsr(userInfoDTO.getLastUseProfileId())
                    .regUsr(userInfoDTO.getLastUseProfileId())
                    .build();
            songBookmarkRepository.save(entity);
        } catch (Exception e){
            logger.error("####### ERROR SONGBOOKMARK #######");
        }
    }

    @Transactional
    public void deleteSongBookmark(Long profileId,Long songId){
        logger.info("#### DELETE SONG BOOKMARK");
        songBookmarkRepository.deleteSongBookmarksByProfileIdAndSongId(profileId,songId);
    }

    @Transactional
    public void deleteSongBookmark(Long songId){
        logger.info("#### DELETE SONG BOOKMARK");
        songBookmarkRepository.deleteSongBookmarksBySongId(songId);
    }

    @Transactional
    public void deleteSongProject(Long projId,Long songId){
        logger.info("#### DELETE SONG PROJECT");
        projectSongRepository.deleteProjectSongByProjIdAndSongId(projId, songId);
    }
    @Transactional
    public void deleteSong(Long songId){
        logger.info("#### DELETE SONG");
        songRepository.deleteById(songId);
    }

    @Transactional
    public void songUpdateByVersion(SongUpdateVersionRequest songUpdateVersionRequest){
        logger.info("####songUpdateByVersion IN <<< {}",songUpdateVersionRequest);
        SongBaseDTO songBaseDTO = SongBaseDTO.builder()
                .songId(songUpdateVersionRequest.getSongId())
                .songAvailYn(songUpdateVersionRequest.getSongAvailYn())
                .modUsr(songUpdateVersionRequest.getProfileId())
                .modDt(LocalDateTime.now())
                .build();
        songRepository.songDetailUpdate(songBaseDTO);
        logger.debug("#### songAvailableCheck");
    }
}
