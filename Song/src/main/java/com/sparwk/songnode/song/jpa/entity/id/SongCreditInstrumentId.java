package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongCreditInstrumentId implements Serializable {
    private Long songCreditSeq;
    private String roleCd;
}
