package com.sparwk.songnode.song.biz.v1.SongCredit.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCreditProfileRequest {

    @Schema(description = "ownerProfileId")
    private Long ownerProfileId;
    @Schema(description = "참여할 song Id")
    private Long songId;
    @Schema(description = "profile Id")
    private Long profileId;

    @Schema(description = "ipn number")
    private String ipnNumber;
    @Schema(description = "nro profileId")
    private Long nroProfileId;
    @Schema(description = "nroNumber")
    private String nroNumber;


    @Schema(description = "rolcd List")
    private String roleCdList;
}
