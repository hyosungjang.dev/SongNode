package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataCustomDto {

    @Schema(description = "노래 아이디")
    private Long songId;
    @Schema(description = "메타 데이터 시퀀스")
    private Long metadataSeq;
    @Schema(description = "메타 데이터 명칭")
    private String metadataName;
    @Schema(description = "메타데이터 값")
    private String metadataValue;

}
