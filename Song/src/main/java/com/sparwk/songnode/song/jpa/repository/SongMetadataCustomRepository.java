package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongMetadataCustom;
import com.sparwk.songnode.song.jpa.repository.dsl.SongMetadataCustomRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongMetadataCustomRepository extends JpaRepository<SongMetadataCustom, Long> , SongMetadataCustomRepositoryDsl {

    List<SongMetadataCustom> findBySongId(Long songId);
    List<SongMetadataCustom> findSongMetadataCustomsBySongId(Long songId);
    Long deleteSongMetadataCustomBySongId(Long songId);
    Long deleteSongMetadataCustomBySongIdAndMetadataSeq(Long songId, Long metadataSeq);
}
