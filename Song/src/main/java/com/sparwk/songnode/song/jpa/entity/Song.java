package com.sparwk.songnode.song.jpa.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song")
public class Song {

    @Id
    @Column(name = "song_id")
    private Long songId;
    @Column(name = "original_song_id", nullable = true)
    private Long originalSongId;
    @Column(name = "song_version_cd", nullable = true)
    private String songVersionCd;
    @Column(name = "song_owner", nullable = true)
    private Long songOwner;
    @Column(name = "lang_cd", nullable = true)
    private String langCd;
    @Column(name = "song_title", nullable = true)
    private String songTitle;
    @Column(name = "song_sub_title", nullable = true)
    private String songSubTitle;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "avatar_img_use_yn", nullable = true)
    private String avatarImgUseYn;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Column(name = "song_avail_yn", nullable = true)
    private String songAvailYn;
    @Column(name = "song_status_cd", nullable = true)
    private String songStatusCd;
    @Column(name = "song_status_mod_dt", nullable = true)
    private LocalDateTime songStatusModDt;
    @Column(name = "user_define_version", nullable = true)
    private String userDefineVersion;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Song(
            Long songId,
            Long originalSongId,
            String songVersionCd,
            Long songOwner,
            String langCd,
            String songTitle,
            String songSubTitle,
            String description,
            String avatarImgUseYn,
            String avatarFileUrl,
            String songAvailYn,
            String songStatusCd,
            LocalDateTime songStatusModDt,
            String userDefineVersion,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.originalSongId = originalSongId;
        this.songVersionCd = songVersionCd;
        this.songOwner = songOwner;
        this.langCd = langCd;
        this.songTitle = songTitle;
        this.songSubTitle = songSubTitle;
        this.description = description;
        this.avatarImgUseYn = avatarImgUseYn;
        this.avatarFileUrl = avatarFileUrl;
        this.songAvailYn = songAvailYn;
        this.songStatusCd = songStatusCd;
        this.songStatusModDt = songStatusModDt;
        this.userDefineVersion = userDefineVersion;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }


//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SongCredit.class)
//    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
//    private List<SongCredit> songCredit;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SongFile.class)
//    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
//    private List<SongFile> songFile;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SongLyrics.class)
//    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
//    private List<SongLyrics> songLyrics;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SongMetadata.class)
//    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
//    private List<SongMetadata> songMetadata;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SongMetadataCustom.class)
//    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
//    private List<SongMetadataCustom> songMetadataTitle;


}
