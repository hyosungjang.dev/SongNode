package com.sparwk.songnode.song.biz.v1.song.dto;


import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditRequest;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.MainSubGenrDto;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterRequest;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongRequest {

    private Long projId;

    private SongDto songDto;

    private CowriterRequest cowriterDto;

    private SongCreditRequest songCreditDto;

    private MainSubGenrDto mainSubGenrDto;

    private SongFileDto songFileDto;

//    private SongLyricsLangRequest songLyricsLangDto;

}


