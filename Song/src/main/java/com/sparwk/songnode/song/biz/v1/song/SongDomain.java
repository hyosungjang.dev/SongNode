package com.sparwk.songnode.song.biz.v1.song;

import com.sparwk.songnode.song.biz.v1.SongCredit.SongCreditService;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditDetailResponse;
import com.sparwk.songnode.song.biz.v1.projectSong.ProjectSongService;
import com.sparwk.songnode.song.biz.v1.projectSong.dto.ProjectSongCountResponse;
import com.sparwk.songnode.song.biz.v1.projectSong.dto.ProjectSongListResponse;
import com.sparwk.songnode.song.biz.v1.song.dto.*;
import com.sparwk.songnode.song.biz.v1.songCowriter.SongCowriterService;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterDetailResponse;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterResponse;
import com.sparwk.songnode.song.biz.v1.songFile.SongFileService;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.SongLyricsService;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.songMetadata.SongMetadataService;
import com.sparwk.songnode.song.biz.v1.timeline.SongTimelineService;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SongDomain {

    private final SongService songService;
    private final SongFileService songFileService;
    private final SongLyricsService songLyricsService;
    private final SongCowriterService songCowriterService;
    private final SongCreditService songCreditService;
    private final SongMetadataService songMetadataService;

    private final ProjectSongService projectSongService;

    private final SongTimelineService songTimelineService;
    private Logger logger = LoggerFactory.getLogger(SongDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;

    /**
     * Project Song Count
     * @param projId
     * @return
     */
    public Response<List<ProjectSongCountResponse>> findProjectSongCount(Long projId) {
        logger.info("#### DOMAIN :: findProjectSongCount IN <<<<< {} ", projId);
        Response<List<ProjectSongCountResponse>> res = new Response<>();
        List<ProjectSongCountResponse> list = projectSongService.findProjectSongCount(projId);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    /**
     * create song sequence
     * @param req
     * @return
     */
    public Response<Long> initSongSeq(HttpServletRequest req) {
        logger.info("##### initSongSeq ##### ");
        Response<Long> res = new Response<>();
        Long songId = songService.initSongSeq();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(songId);
        return res;
    }

    /**
     * project - song connection
     * @param dto
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> projectSongSaveService(SongRequest dto, HttpServletRequest req) {
        logger.info("##### projectSongSaveService ##### >>> {}", dto);
        Response<Void> res = new Response<>();
        // validation
        String songValid = songService.createSongValid(dto, req);
        if(!StringUtils.equals(songValid, ResultCodeConst.SUCCESS.getCode())) {
            res.setResultCd(songValid);
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // save진행
        SongBaseDTO result = songService.projectSongSaveService(dto);

        // save 실패
        if(result == null) {
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // 장르, 서브장르 추가
        songService.songSaveGenre(dto.getMainSubGenrDto(), result.getSongId());
        // 프로젝트 song 추가
        songService.saveSongProject(dto.getProjId(), result.getSongId());
        // 프로젝트 song cowriter 추가
        songCowriterService.saveSongCowriter(dto.getCowriterDto(), result.getSongId());
        // song credit 추가
        songCreditService.saveSongCredit(dto.getSongCreditDto(), result.getSongId());
        // song file 추가
        SongFileBaseDTO file = songFileService.songFileSaveService(dto.getSongFileDto());
        // song file에 해당하는 lylics 추가
        if(file != null){
            songLyricsService.songLyricsSaveService(result.getSongId(), file.getSongFileSeq());
        }

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                        .songId(result.getSongId())
                        .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                        .resultMsg(result.getSongTitle() + "Song has been created.\n")
                .build());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * song List
     * @param projId
     * @param offset
     * @param limit
     * @param req
     * @return
     */
    public Response<List<SongResponse>> selectSongList(Long projId, Long profileId, int offset , int limit, HttpServletRequest req) {
        logger.info("##### selectSongList ##### ");
        Response<List<SongResponse>> res = new Response<>();

        List<SongResponse> result =  songService.selectSongList(projId,profileId, offset, limit);

        for(int i=0 ; i<result.size() ; i++){
            List<SongMetadataBaseDTO> songMetadataBaseDTOS = songService.selectSongMetadata(result.get(i).getSongId());
            if(songMetadataBaseDTOS != null){
                result.get(i).setSongMetadataBaseDTOs(songMetadataBaseDTOS);
            }
            List<CowriterResponse> cowriterResponses = songCowriterService.selectSongCowriters(result.get(i).getSongId());
            if( cowriterResponses != null ) {
                result.get(i).setCowriterResponses(cowriterResponses);
            }
            List<SongFileDto> fileList = songFileService.songFileSelectService(result.get(i).getSongId());
            if(fileList != null){
                result.get(i).setSongFileDtos(fileList);
            }
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());

        if(result == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
        }
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }


    /**
     * SONG DETAIL UPDATE BY ORIGINAL_SONG_ID
     * @param songModifyRequest
     * @return
     */
    public Response<Void> modifySongDetail(SongModifyRequest songModifyRequest) {
        logger.info("##### MODIFY SONG DETAIL #####  >>>>> {} ", songModifyRequest);
        Response<Void> res = new Response<>();

        songService.modifySongDetail(songModifyRequest);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    /**
     * SONG VERSION LIST
     * @param projId
     * @param originalSongId
     * @param req
     * @return
     */
    public Response<List<SongResponse>> selectSongVersionList(Long projId, Long originalSongId, HttpServletRequest req) {
        logger.info("##### selectSongVersionList ##### ");
        Response<List<SongResponse>> res = new Response<>();
        List<SongResponse> result =  songService.selectSongVersionList(projId, originalSongId);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * song detail for projId, songId
     * @param projId
     * @param songId
     * @param req
     * @return
     */
    public Response<SongDetailResponse> getSongDetail(Long projId, Long songId, HttpServletRequest req) {
        logger.info("##### getSongDetail ##### ");
        Response<SongDetailResponse> res = new Response<>();
        SongDetailResponse result = new SongDetailResponse();

        SongDto songResponse = songService.selectSongDetailInfo(projId, songId);

        if(songResponse == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // song detail
        result.setSongResponse(songResponse);

//        songMetadataService.selectSongMetadataList(songId);
//        songMetadataService.selectSongMetadataCustomList(songId);
        // song file
        List<SongFileDto> songFileDto = songFileService.songFileSelectService(songId);
        // song lyrics
        SongLyricsDto songLyricsDto = songLyricsService.songLyricsSelectService(songId, songFileDto.get(0).getSongFileSeq());

        List<SongLyricsLangBaseDTO> songLyricsLangBaseDTOS = songLyricsService.selectSongLyricsLang(songId);
        // cowriter 정보
//        List<CowriterResponse> cowriterResponseList = songCowriterService.selectSongCowriters(songId);
        List<CowriterDetailResponse> cowriterResponseList = songCowriterService.selectSongCowirterDetailList(songId);
        // credit 정보
        List<SongCreditDetailResponse> creditList = songCreditService.songCreditList(songId);

        // response setting
        if(songFileDto.size() > 0){
            result.setSongFileDtos(songFileDto);
        }
        if(songLyricsDto != null){
            result.setSongLyricsDto(songLyricsDto);
        }
        if(songLyricsLangBaseDTOS != null) {
            result.setSongLyricsLangResponse(songLyricsLangBaseDTOS);
        }
        if(cowriterResponseList.size() > 0){
            result.setCowriterResponses(cowriterResponseList);
        }
        if(creditList.size() > 0){
            result.setSongCreditResponses(creditList);
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }


    /**
     * song copy
     * @param dto
     * @param req
     * @return
     */
    @Transactional
    public Response<Void> copySongVersion(SongVersionRequest dto, HttpServletRequest req) {
        logger.info("##### Domain: copySongVersion ##### ");
        Response<Void> res = new Response<>();

        try {
            // song copy
            songService.songCopy(dto);
            // song project add
            songService.saveSongProject(dto.getProjId(), dto.getNewSongId());
            // song cowriter
            songCowriterService.cowriterCopy(dto);
            // song credit(추후)
            songCreditService.songCreditCopy(dto);
            // song file copy
            songFileService.songFileCopy(dto);
            // song lyrics lang
            songLyricsService.copySongLyricsLang(dto);
            // song metadata
            songMetadataService.songMetadataCopy(dto);
            // song customCopy
            songMetadataService.songMetadataCustomCopy(dto);

        } catch (Exception e){
            logger.error("#### COPY ERROR #####", e.getMessage());
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * bookmark
     * @param songBookmarkRequest
     * @param req
     * @return
     */
    public Response<Void> songBookmark(SongBookmarkRequest songBookmarkRequest, HttpServletRequest req) {
        logger.info("##### Domain: songBookmark ##### ");
        Response<Void> res = new Response<>();
        songService.songBookmark(songBookmarkRequest);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response<Void> deleteSongVersion(SongVersionDeleteRequest songVersionDeleteRequest) {
        logger.info("#### DOMAIN :: deleteSongVersion IN <<<<<{} ", songVersionDeleteRequest);
        Response<Void> res = new Response<>();
        Long songId = songVersionDeleteRequest.getSongId();
        Long projId = songVersionDeleteRequest.getProjId();

        // song bookmark
        songService.deleteSongBookmark(songId);
        // song lyrics, file
        songFileService.deleteSongFileAll(songId);
        // song lyrics lang delete
        songLyricsService.deleteSongLyricsLangAll(songId);
        // song metadata
        songMetadataService.deleteSongMetadata(songId);
        // song custom
        songMetadataService.deleteSongMetadataCustom(songId);
        // cowriter
        songCowriterService.deleteCowriterAll(songId);
        // credit
        songCreditService.deleteSongCreditAll(songId);
        // song project
        songService.deleteSongProject(projId, songId);
        // song
        songService.deleteSong(songId);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * Avail
     * @param songUpdateVersionRequest
     * @return
     */
    public Response<Void> songUpdateByVersion(SongUpdateVersionRequest songUpdateVersionRequest){
        Response<Void> res = new Response<>();
        songService.songUpdateByVersion(songUpdateVersionRequest);
        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(songUpdateVersionRequest.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("song has become available.")
                .build());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     *
     * @return
     */
    public Response<List<SongTimelineBaseDTO>> songTimeline(Long songId) {
        Response<List<SongTimelineBaseDTO>> res = new Response<>();
        List<SongTimelineBaseDTO> result = songTimelineService.selectSongTimeline(songId);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }
}
