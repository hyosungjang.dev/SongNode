package com.sparwk.songnode.song.jpa.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_timeline")
public class SongTimeline {

    @Id
    @GeneratedValue(generator = "tb_song_timeline_seq")
    @Column(name = "timeline_seq", nullable = true)
    private Long timelineSeq;
    @Column(name = "song_id", nullable = true)
    private Long songId;

    @Column(name = "component_use_id", nullable = true)
    private Long componentUseId;
    @Column(name = "componet_cd", nullable = true)
    private String componetCd;
    @Column(name = "tlm_cd", nullable = true)
    private String tlmCd;

    @Column(name = "params", nullable = true, columnDefinition = "TEXT[]")
    @ElementCollection
    private List<String> params;
    @Column(name = "result_msg", nullable = true)
    private String resultMsg;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;
    @Builder
    SongTimeline(
        Long timelineSeq,
        Long songId,
        Long componentUseId,
        String componetCd,
        String tlmCd,
        List<String> params,
        String resultMsg,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
         this.timelineSeq       =           timelineSeq;
         this.songId            =           songId;
         this.componentUseId    =           componentUseId;
         this.componetCd        =           componetCd;
         this.tlmCd             =           tlmCd;
         this.params            =           params;
         this.resultMsg         =           resultMsg;
         this.regUsr            =           regUsr;
         this.regDt             =           regDt;
         this.modUsr            =           modUsr;
         this.modDt             =           modDt;
    }


}
