package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongCreditId implements Serializable {
    private Long songCreditSeq;
    private Long songId;
    private Long profileId;
}
