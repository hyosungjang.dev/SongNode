package com.sparwk.songnode.song.biz.v1.songCowriter.dto;


import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CowriterRequest {

    @Schema(description = "참여할 song Id")
    private Long songId;
    @Schema(description = "협업자 프로필 아이디 리스트")
    private String profileList;
    @ApiModelProperty(name = "songCowriterSeq", notes = "Cowriter Delete에서만 사용")
    @Schema(description = "song cowriter 순서")
    private Long songCowriterSeq;
}
