package com.sparwk.songnode.song.biz.v1.songFile.dto;

import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongFileAndLyricsResponse {
    @Schema(description = "노래 파일")
    private SongFileDto songFileDto;
    @Schema(description = "노래 가사")
    private SongLyricsDto songLyricsDto;
}
