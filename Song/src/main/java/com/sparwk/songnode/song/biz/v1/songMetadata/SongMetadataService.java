package com.sparwk.songnode.song.biz.v1.songMetadata;

import com.sparwk.songnode.song.biz.v1.song.dto.SongVersionRequest;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataCustomDto;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataCustomRequest;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataDto;
import com.sparwk.songnode.song.biz.v1.songMetadata.dto.SongMetadataRequest;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import com.sparwk.songnode.song.jpa.SongServiceBaseRepository;
import com.sparwk.songnode.song.jpa.dto.SongMetadataBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongMetadata;
import com.sparwk.songnode.song.jpa.entity.SongMetadataCustom;
import com.sparwk.songnode.song.jpa.repository.SongMetadataCustomRepository;
import com.sparwk.songnode.song.jpa.repository.SongMetadataRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongMetadataService {

    private final SongMetadataRepository songMetadataRepository;
    private final SongMetadataCustomRepository songMetadataCustomRepository;

    private final SongServiceBaseRepository songServiceBaseRepository;// 삭제예정

    private final ModelMapper modelMapper;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(SongMetadataService.class);

    /**
     * hyosungjang
     * song metadata list
     * @param songId
     * @return
     */
    public List<SongMetadataDto> selectSongMetadataList(Long songId) {
        List<SongMetadataDto> songMetadataList = null;

        try {

           songMetadataList = songMetadataRepository.findSongMetadataBySongId(songId).stream()
                    .map(e -> modelMapper.map(e, SongMetadataDto.class))
                    .collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("#### songMetadataList Error ####");
        }
        return songMetadataList;
    }

    /**
     * hyosungjang
     * song metadata custom list
     * @param songId
     * @return
     */
    public List<SongMetadataCustomDto> selectSongMetadataCustomList(Long songId) {
        List<SongMetadataCustomDto> songMetadataCustomList = null;
        try {

           songMetadataCustomList = songMetadataCustomRepository.findSongMetadataCustomsBySongId(songId).stream()
                    .map(e -> modelMapper.map(e, SongMetadataCustomDto.class))
                    .collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("#### songMetadataList Error ####");
        }

        return songMetadataCustomList;
    }


    /**
     * custom song metadata insert
     * @param dto
     */
    @Transactional
    public void saveSongMetadata(SongMetadataRequest dto) {
        logger.info("##### saveSongMetadata ENTER ######");
        try {
            Long songId = dto.getSongId();

            if(!StringUtils.isEmpty(dto.getGenresList())) {
                Arrays.stream(dto.getGenresList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(s, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getGenresCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getSubGenresList())) {
                Arrays.stream(dto.getSubGenresList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getSubGenresCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getThemesList())) {
                Arrays.stream(dto.getThemesList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getTempoCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getMoodsList())) {
                Arrays.stream(dto.getMoodsList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getMoodsCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getVocalList())) {
                Arrays.stream(dto.getVocalList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getVocalCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getEraList())) {
                Arrays.stream(dto.getEraList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getEraCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getTempoList())) {
                Arrays.stream(dto.getTempoList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getTempoCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

            if(!StringUtils.isEmpty(dto.getInstrumentsList())) {
                Arrays.stream(dto.getInstrumentsList().split(",")).forEach(s -> {
                    SongMetadata entity = modelMapper.map(dto, SongMetadata.class);
                    entity.setSongId(songId);
                    entity.setAttrTypeCd(dto.getInstrumentsCd());
                    entity.setAttrDtlCd(s);
                    entity.setRegUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setModDt(LocalDateTime.now());
                    songMetadataRepository.save(entity);
                });
            }

        } catch (Exception e) {
            logger.error("##### SongMetadataRequest SAVE ERROR");
        }
    }


    /**
     * custom song metadata insert
     * @param dto
     */
    @Transactional
    public void saveSongMetadataCustom(SongMetadataCustomRequest dto) {
        logger.info("##### saveSongMetadataCustom ENTER ######");
        try {
            if(dto.getSongMetadataCustomDtoList().size() > 0){
                for(SongMetadataCustomDto vo : dto.getSongMetadataCustomDtoList()){
                    SongMetadataCustom entity = modelMapper.map(vo, SongMetadataCustom.class);
                    entity.setModDt(LocalDateTime.now());
                    entity.setModUsr(dto.getProfileId());
                    entity.setRegDt(LocalDateTime.now());
                    entity.setRegUsr(dto.getProfileId());
                    entity = songMetadataCustomRepository.save(entity);
                }
            }

        } catch (Exception e) {
            logger.error("##### SONG SAVE ERRROR");
        }
    }

    /**
     * custom song metadata delete
     * @param songId
     */
    @Transactional
    public void deleteSongMetadata(Long songId) {
        logger.info("##### deleteSongMetadata ENTER ######");
        try {
            songMetadataRepository.deleteSongMetadataBySongId(songId);
        } catch (Exception e) {
            logger.error("##### SONG SAVE ERRROR");
        }
    }

    /**
     * custom song metadata custom delete
     * @param songId
     */
    @Transactional
    public void deleteSongMetadataCustom(Long songId) {
        logger.info("##### deleteSongMetadataCustom ENTER ######");
        try {
            songMetadataCustomRepository.deleteSongMetadataCustomBySongId(songId);
        } catch (Exception e) {
            logger.error("##### SONG SAVE ERRROR");
        }
    }


    /**
     * custom song metadata insert
     * @param dto
     */
//    @Transactional
//    public void updateSongMetadataCustom(SongMetadataCustomDto dto) {
//        logger.info("##### updateSongMetadataCustom ENTER ######");
//        try {
//            SongMetadataCustom entity = modelMapper.map(dto, SongMetadataCustom.class);
//            entity.setModDt(LocalDateTime.now());
//            entity.setModUsr(dto.getProfileId());
//            songMetadataCustomRepository.saveSongMetadataCustom(entity);
//
//        } catch (Exception e) {
//            logger.error("##### SONG SAVE ERROR");
//        }
//    }

    /**
     * song metadata copy
     * @param vo
     */
    @Transactional
    public void songMetadataCopy(SongVersionRequest vo) {
        logger.info("##### songMetadataCopy #####");
        List<SongMetadataDto> list = selectSongMetadataList(vo.getSongId());
        if(list.size() > 0) {
            list.stream().forEach(e -> {
                e.setSongId(vo.getNewSongId());
                SongMetadata entity = modelMapper.map(e, SongMetadata.class);
                entity.setRegUsr(Long.valueOf(1));
                entity.setRegDt(LocalDateTime.now());
                entity.setModUsr(Long.valueOf(1));
                entity.setModDt(LocalDateTime.now());
                entity = songMetadataRepository.save(entity);
            });
        }
    }


    /**
     * songMetadataCustom modify
     * @param songMetadataCustomDto
     */
    @Transactional
    public void deleteSongMetadataCustom(SongMetadataCustomDto songMetadataCustomDto) {
        logger.info("### ENTER deleteSongMetadataCustom >>>>> {}",songMetadataCustomDto);
        try {
            Long songId = songMetadataCustomDto.getSongId();
            Long metadataSeq = songMetadataCustomDto.getMetadataSeq();
            songMetadataCustomRepository.deleteSongMetadataCustomBySongIdAndMetadataSeq(songId ,metadataSeq);
        } catch (NullPointerException e){
            logger.error("NullPoint Exception {}", e.getMessage());
        }
    }

    /**
     * songMetadataCustomCopy
     * @param vo
     */
    @Transactional
    public void songMetadataCustomCopy(SongVersionRequest vo) {
        logger.info("##### saveSongMetadataCustom ENTER ######");
        List<SongMetadataCustomDto> list = selectSongMetadataCustomList(vo.getSongId());
        if(list.size() > 0) {
            list.stream().forEach(e -> {
                e.setSongId(vo.getNewSongId());
                SongMetadataCustom entity = modelMapper.map(e, SongMetadataCustom.class);
                entity = songMetadataCustomRepository.save(entity);
            });
        }
    }



    public Response SongMetadataSaveService(SongMetadataDto dto){
        SongMetadataBaseDTO result = songServiceBaseRepository
                .SongMetadataSaveService(modelMapper.map(dto, SongMetadataBaseDTO.class));
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response SongMetadataSelectService(Long songId){
        List<SongMetadataBaseDTO> result = songMetadataRepository.findBySongId(songId)
                .stream().map(e -> modelMapper.map(e, SongMetadataBaseDTO.class))
                .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
