package com.sparwk.songnode.song.biz.v1.songCowriter;

import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterDetailResponse;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterProfileRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.SongCowriterDto;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.AcceptSplitSheetRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.SplitSheetRequest;
import com.sparwk.songnode.song.biz.v1.timeline.SongTimelineService;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongTimelineBaseDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SongCowriterDomain {

    private final SongCowriterService songCowriterService;
    private final SongTimelineService songTimelineService;
    private Logger logger = LoggerFactory.getLogger(SongCowriterDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;

    /**
     * Song Cowriter Save
     * @param cowriterRequest
     * @return
     */
    public Response<Void> saveCowriter(CowriterRequest cowriterRequest){
        Response<Void> res = new Response<>();
        songCowriterService.saveSongCowriter(cowriterRequest, cowriterRequest.getSongId());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * song cowriter delete
     * @param cowriterRequest
     * @return
     */
    public Response<Void> deleteCowriter(CowriterRequest cowriterRequest){
        logger.info("#### DOMAIN :: deleteCowriter #### IN <<<< {}", cowriterRequest);
        Response<Void> res = new Response<>();

        // cowriter delete˙
        songCowriterService.deleteCowriterSp(cowriterRequest);
        songCowriterService.deleteCowriterRole(cowriterRequest);
        songCowriterService.deleteCowriter(cowriterRequest);

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(cowriterRequest.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("Member Split has been deleted.\n" +
                        "Please rewrite the split sheet.\n")
                .build());

        // cowriter delete
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     *
     * @param songId
     * @param profileId
     * @return
     */
    public Response<CowriterDetailResponse> selectSongCowriterDetail(Long songId, Long profileId) {
        logger.info("##### initSongSeq ##### ");
        Response<CowriterDetailResponse> res = new Response<>();

        CowriterDetailResponse result = songCowriterService.selectSongCowirterDetail(songId, profileId);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * Song Cowriter Add Domain
     * @param cowriterRequest
     * @param songId
     * @return
     */
    public Response<Void> addSongCowriterByProjectSongMember(CowriterRequest cowriterRequest ,Long songId) {
        Response<Void> res = new Response<>();
        // Valid Song Cowwriter, SongId
        logger.info("#### SONG COWRITER DOMAIN: addSongCowriterByProjectSongMember >>> cowriterRequest {}", cowriterRequest);
        logger.info("#### SONG COWRITER DOMAIN: addSongCowriterByProjectSongMember >>> songId {}", songId);

        songCowriterService.saveSongCowriter(cowriterRequest, songId);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * split sheet
     * @param splitSheetRequest
     * @return
     */
    public Response<Void> rateShareSplitSheet(SplitSheetRequest splitSheetRequest){
        logger.info("#### DOMAIN rateShareSplitSheet IN <<<<< {} ", splitSheetRequest);
        // valid 체크
        Response<Void> res = new Response<>();
        songCowriterService.rateShareSplitSheet(splitSheetRequest);

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(splitSheetRequest.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("The song Share Split has been saved.\n")
                .build());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * reset splitsheet
     * @param songId
     * @param ownerProfileId
     * @return
     */
    public Response<Void> resetRateShareSplitSheet(Long songId, Long ownerProfileId){
        logger.info("#### resetRateShareSplitSheet #### IN <<<< songId {}, ownerProfileId {} ", songId, ownerProfileId);
        // valid 체크
        Response<Void> res = new Response<>();
        songCowriterService.resetRateShareSplitSheet(songId,ownerProfileId);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * Split Sheet에 대한 수락
     * @param dto
     * @return
     */
    public Response<Void> acceptSplitSheet(AcceptSplitSheetRequest dto){
        logger.info("#### acceptSplitSheet IN <<<<< {}", dto);
        Response<Void> res = new Response<>();
        // split - sheet 처리
        songCowriterService.acceptSplitSheet(dto);

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(dto.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("agreed to {Profile.name}'s split sheet.")
                .build());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    /**
     * set Profile Cowriter
     * @return
     */
    public Response<Void> setProfileCowriter(CowriterProfileRequest dto) {
        logger.info("#### setProfileCowriter IN <<<<< {}", dto);
        Response<Void> res = new Response<>();
        songCowriterService.setProfileCowriter(dto);

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(dto.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("Profile information has been saved.")
                .build());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response<SongCowriterDto> countAcceptCowriter(Long songId){
        Response<SongCowriterDto> res = new Response<>();
        //song에 관련 acceptyn 체크
        SongCowriterDto result = songCowriterService.songAvailableCheck(songId);
        res.setResultCd(ResultCodeConst.NOT_REQUIRE_SONG_AVAILABLE_ACCEPT.getCode());
        if(result != null){
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResult(result);
        }
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
