package com.sparwk.songnode.song.biz.v1.song.dto;


import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongUpdateVersionRequest {

    @ApiModelProperty(value = "profileId", notes = "profileId" , example = "1")
    @Schema(description = "프로필ID")
    private Long profileId;

    @ApiModelProperty(value = "songId", notes = "SongId" , example = "1")
    @Schema(description = "노래 아이디")
    private Long songId;

    @ApiModelProperty(value = "song availYn", notes = "availYn" , example = "Y/N")
    @Schema(description = "song availYn")
    private String songAvailYn;
}