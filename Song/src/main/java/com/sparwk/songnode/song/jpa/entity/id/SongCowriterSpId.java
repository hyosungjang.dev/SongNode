package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongCowriterSpId implements Serializable {
    private Long songCowriterSeq;
    private Long spProfileId;
}
