package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongLyricsLangBaseDTO {

        private Long songId;
        private String langCd;
        private Long regUsr;
        private LocalDateTime regDt;
        private Long modUsr;
        private LocalDateTime modDt;
}
