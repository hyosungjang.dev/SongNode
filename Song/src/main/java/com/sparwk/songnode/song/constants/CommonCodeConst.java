package com.sparwk.songnode.song.constants;

public class CommonCodeConst {

	public final static class YN {
		public final static String Y = "Y";
		public final static String N = "N";
	}

	public final static class MODIFY {
		public final static String UPDATE 	= "U";
		public final static String DELETE 	= "D";
		public final static String RESET 	= "R";
	}

	public final static class SONG_TIMELINE_CODE {
		public final static String PROJECT 					= "CMN000009";
		public final static String SONG 					= "CMN000014";
		public final static String SONG_LIBRARY 			= "CMN000015";
	}

	public final static class SONG_VERSION_CD {
		public final static String ORIGIAL_VERSION 			= "SGV000001";
		public final static String ACAPPELLA_VERSION 		= "SGV000002";
		public final static String ALBUM_VERSION 			= "SGV000003";
		public final static String ALTERNATIVE_VERSION 		= "SGV000004";
		public final static String CLEAN_VERSION 			= "SGV000005";
		public final static String DEMO_VERSION 			= "SGV000006";
		public final static String EDITED_VERSION 			= "SGV000007";
		public final static String INSTRUMENTAL_VERSION 	= "SGV000008";
		public final static String KARAOKE_VERSION 			= "SGV000009";
		public final static String LIVE_VERSION 			= "SGV000010";
		public final static String MIX_VERSION 				= "SGV000011";
		public final static String MONO_VERSION 			= "SGV000012";
		public final static String RADIO_VERSION 			= "SGV000013";
		public final static String REMIX_VERSION 			= "SGV000014";
		public final static String SESSION_VERSION 			= "SGV000015";
		public final static String SINGLE_VERSION 			= "SGV000016";
		public final static String STEREO_VERSION 			= "SGV000017";
		public final static String USER_DEFINED 			= "SGV999999";
	}

	public final static class SONG_STATUS_CD {
		public final static String AVAILABLE	= 	"SST000001";
		public final static String HOLD			= 	"SST000003";
		public final static String CUT			= 	"SST000002";
		public final static String PASS			= 	"SST000004";
		public final static String UNRELEASE	= 	"SST000005";
		public final static String RELEASED		= 	"SST000006";
	}
}