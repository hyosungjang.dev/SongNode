package com.sparwk.songnode.song.biz.v1.songMetadata;

import com.sparwk.songnode.song.biz.v1.songMetadata.dto.*;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SongMetadataDomain {

    private final SongMetadataService songMetadataService;
    private Logger logger = LoggerFactory.getLogger(SongMetadataDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;


    /**
     * song metadata List
     * @param songId
     * @param req
     * @return
     */
    public Response<SongMetadataResponse> getSongMetadata(Long songId, HttpServletRequest req) {
        Response<SongMetadataResponse> res = new Response<>();
        SongMetadataResponse result = new SongMetadataResponse();

        List<SongMetadataDto> songMetadataList = songMetadataService.selectSongMetadataList(songId);
        List<SongMetadataCustomDto> songMetadataCustomList = songMetadataService.selectSongMetadataCustomList(songId);
        if(songMetadataList != null) {
            result.setSongMetadataList(songMetadataList);
        }

        if(songMetadataCustomList != null) {
            result.setSongMetadataCustomList(songMetadataCustomList);
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * SONG metadata custom save
     * @param songMetadataRequest
     * @param req
     * @return
     */
    public Response<Void> modifySongMetadata(SongMetadataRequest songMetadataRequest , HttpServletRequest req ){
        logger.info("### DOMAIN :: modifySongMetadata IN <<<<<< {}", songMetadataRequest);
        Response<Void> res = new Response<>();

        songMetadataService.deleteSongMetadata(songMetadataRequest.getSongId());
        songMetadataService.saveSongMetadata(songMetadataRequest);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * SONG metadata custom save
     * @param songMetadataCustomRequest
     * @return
     */
    public Response<Void> saveSongMetadataCustom(SongMetadataCustomRequest songMetadataCustomRequest){
        logger.info("### DOMAIN :: saveSongMetadataCustom IN <<<<<< {}", songMetadataCustomRequest);
        Response<Void> res = new Response<>();

        songMetadataService.deleteSongMetadataCustom(songMetadataCustomRequest.getSongId());
        songMetadataService.saveSongMetadataCustom(songMetadataCustomRequest);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * SONG metadata custom save
     * @param songMetadataCustom
     * @param req
     * @return
     */
//    public Response<Void> updateSongMetadataCustom(SongMetadataCustomDto songMetadataCustom ,HttpServletRequest req ){
//        Response<Void> res = new Response<>();
//        songMetadataService.updateSongMetadataCustom(songMetadataCustom);
//        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
//        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//        return res;
//    }


    /**
     * SONG metadata custom delete
     * @param songMetadataCustom
     * @param req
     * @return
     */
    public Response<Void> deleteSongMetadataCustom(SongMetadataCustomDto songMetadataCustom, HttpServletRequest req ){
        Response<Void> res = new Response<>();
        songMetadataService.deleteSongMetadataCustom(songMetadataCustom);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
