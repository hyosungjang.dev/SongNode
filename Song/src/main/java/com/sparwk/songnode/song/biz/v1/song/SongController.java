package com.sparwk.songnode.song.biz.v1.song;

import com.sparwk.songnode.song.biz.v1.projectSong.dto.ProjectSongCountResponse;
import com.sparwk.songnode.song.biz.v1.song.dto.*;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.jpa.dto.SongTimelineBaseDTO;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = Environment.API_VERSION)
@CrossOrigin("*")
@Api(tags = "Song Server")
@RequiredArgsConstructor
public class SongController {

    final private SongDomain songDomain;
    private final Logger logger = LoggerFactory.getLogger(SongController.class);



    @ApiOperation(
            value = "project Song Count"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/project/count/{projId}")
    public Response<List<ProjectSongCountResponse>> findProjectSongCount(
            @PathVariable(name = "projId") Long projId
    ) {
        logger.info("#### findProjectSongCount ####");
        return songDomain.findProjectSongCount(projId);
    }

    @ApiOperation(
            value = "Song Init Seq"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/init/seq")
    public Response<Long> initSongSeq(HttpServletRequest req) {
        logger.info("#### initSongSeq ####");
        return songDomain.initSongSeq(req);
    }

    @ApiOperation(
            value = "Song"
    )
    @Operation(
            description = "projectSong 에서 생성시 사용",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "송 Id 생성")
            }
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=2000, message = "저장실패"),
            @ApiResponse(code=6020, message = "프로젝트가 존재하지 않음"),
            @ApiResponse(code=6100, message = "SONG 파라미터가 불충분"),
    })
    @PostMapping(path = "/save")
    public Response<Void> projectSongSaveController(@Valid @RequestBody SongRequest dto, HttpServletRequest req) {
        logger.info("#### projectSongSaveController ####");
        return songDomain.projectSongSaveService(dto, req);
    }


    @ApiOperation(
            value = "Song List"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "profileId", value = "profile ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "offset", value = "검색 시작(0 부터시작)", dataType = "int", paramType = "path", required = true),
            @ApiImplicitParam(name = "limit", value = "검색 끝(~까지 )", dataType = "int", paramType = "path", required = true),
            @ApiImplicitParam(name = "sort", value = "검색컬럼(projTitle)", dataType = "String", paramType = "query", required = false),
            @ApiImplicitParam(name = "sortDirection", value = "ASC/DESC", dataType = "int", paramType = "query", required = false),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/list/{projId}/{profileId}/{offset}/{limit}")
    public Response<List<SongResponse>> selectSongList(
            HttpServletRequest req,
            @PathVariable(name = "projId") Long projId,
            @PathVariable(name = "profileId") Long profileId,
            @PathVariable(name = "offset") int offset,
            @PathVariable(name = "limit") int limit,
            @RequestParam(name = "sort", required = false) String sort,
            @RequestParam(name = "sortDirection", required = false) String sortDirection
    ) {
        logger.info("#### selectSongList ####");
        return songDomain.selectSongList(projId, profileId, offset, limit, req);
    }

    @ApiOperation(
            value = "Song List"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "originalSongId", value = "originalSongId", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/version/list/{projId}/{originalSongId}")
    public Response<List<SongResponse>> selectSongVersionList(
            HttpServletRequest req,
            @PathVariable(name = "projId") Long projId,
            @PathVariable(name = "originalSongId") Long originalSongId
    ) {
        logger.info("#### selectSongVersionList ####");
        return songDomain.selectSongVersionList(projId, originalSongId, req);
    }

    @ApiOperation(
            value = "Song List"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "songId", value = "songId", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/detail/{projId}/{songId}")
    public Response<SongDetailResponse> selectSongDetail(
            HttpServletRequest req,
            @PathVariable(name = "projId") Long projId,
            @PathVariable(name = "songId") Long  songId
    ) {
        logger.info("#### selectSongDetail ####");
        return songDomain.getSongDetail(projId, songId, req);
    }


    @ApiOperation(
            value = "Song Detail modify"
    )
    @Operation(
            description = "song detail modify",
            parameters = {
                    @Parameter(name = "songModifyRequest", in = ParameterIn.QUERY, required = true, description = "song detail 수정")
            }
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/modify/detail")
    public Response<Void> updateSongDetail(
            @RequestBody SongModifyRequest songModifyRequest
    ) {
        logger.info("#### songModifyRequest ####");
        return songDomain.modifySongDetail(songModifyRequest);
    }


    @ApiOperation(
            value = "송 version copy"
    )
    @Operation(
            description = "송 version copy",
            parameters = {
                    @Parameter(name = "songVersionRequest", in = ParameterIn.QUERY, required = true, description = "송 copy")
            }
    )
    @PostMapping(path = "/copy")
    public Response<Void> copySongVersion(
            HttpServletRequest req,
            @RequestBody SongVersionRequest songVersionRequest
    ){
        logger.info(" ENTER createSongCopy");
        return songDomain.copySongVersion(songVersionRequest,req);
    }

    @ApiOperation(
            value = "송 version 삭제"
    )
    @Operation(
            description = "송 version 삭제",
            parameters = {
                    @Parameter(name = "songVersionDeleteRequest", in = ParameterIn.PATH, required = true, description = "송 copy")
            }
    )
    @PostMapping(path = "/delete/version")
    public Response<Void> deleteSongVersion(
           @RequestBody SongVersionDeleteRequest songVersionDeleteRequest
    ){
        logger.info("### ENTER createSongCopy IN <<<<< {}", songVersionDeleteRequest);
        return songDomain.deleteSongVersion(songVersionDeleteRequest);
    }


    @ApiOperation(
            value = "송 북마크"
    )
    @Operation(
            description = "송 북마크",
            parameters = {
                    @Parameter(name = "songBookmarkRequest", in = ParameterIn.QUERY, required = true, description = "송 북마크")
            }
    )
    @PostMapping(path = "/bookmark")
    public Response<Void> copySongVersion(
            HttpServletRequest req,
            @RequestBody SongBookmarkRequest songBookmarkRequest
    ){
        logger.info(" ####ENTER songBookmarkRequest");
        return songDomain.songBookmark(songBookmarkRequest,req);
    }


    @ApiOperation(
            value = "송 detail version 수정"
    )
    @Operation(
            description = "송 Avaliable Yn check",
            parameters = {
                    @Parameter(name = "songUpdateVersionRequest", in = ParameterIn.QUERY, required = true, description = "송 북마크")
            }
    )
    @PostMapping(path = "/avail/modify")
    public Response<Void> updateAvailSongDetail(
            @RequestBody SongUpdateVersionRequest songUpdateVersionRequest
    ){
        logger.info(" ####ENTER songUpdateVersionRequest ### {}",songUpdateVersionRequest);
        return songDomain.songUpdateByVersion(songUpdateVersionRequest);
    }

    @ApiOperation(
            value = "송 detail version 수정"
    )
    @Operation(
            description = "송 Avaliable Yn check",
            parameters = {
                    @Parameter(name = "songUpdateVersionRequest", in = ParameterIn.QUERY, required = true, description = "송 북마크")
            }
    )
    @PostMapping(path = "/timeline/{songId}")
    public Response<List<SongTimelineBaseDTO>> selectSongtimeline(
            @PathVariable(name = "songId") Long songId
    ){
        logger.info(" ####ENTER songUpdateVersionRequest ### {}",songId);
        return songDomain.songTimeline(songId);
    }
}
