
package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongCreditInstrumentId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongCreditInstrumentId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_credit_instrument")
public class SongCreditInstrument {


    @Id
    @Column(name = "song_credit_seq", nullable = true)
    private Long songCreditSeq;
    @Id
    @Column(name = "role_cd", nullable = true)
    private String roleCd;

    @Builder
    SongCreditInstrument(
            Long songCreditSeq,
            String roleCd
    ) {
        this.songCreditSeq = songCreditSeq;
        this.roleCd = roleCd;
    }

}
