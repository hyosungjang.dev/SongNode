package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongFileId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_file")
@IdClass(SongFileId.class)
public class SongFile extends BaseEntity {

    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @GeneratedValue(generator = "tb_song_file_seq")
    @Column(name = "song_file_seq", nullable = true)
    private Long songFileSeq;
    @Column(name = "song_file_name", nullable = true)
    private String songFileName;
    @Column(name = "song_file_path", nullable = true)
    private String songFilePath;
    @Column(name = "song_file_size", nullable = true)
    private Long songFileSize;
    @Column(name = "song_file_comt", nullable = true)
    private String songFileComt;
    @Column(name = "bpm", nullable = true)
    private Long bpm;
    @Column(name = "contain_sample_yn", nullable = true)
    private String containSampleYn;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;
    @Builder
    SongFile(
            Long songId,
            Long songFileSeq,
            String songFileName,
            String songFilePath,
            Long songFileSize,
            String songFileComt,
            Long bpm,
            String containSampleYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.songFileName = songFileName;
        this.songFilePath = songFilePath;
        this.songFileSize = songFileSize;
        this.songFileComt = songFileComt;
        this.bpm = bpm;
        this.containSampleYn = containSampleYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }


}
