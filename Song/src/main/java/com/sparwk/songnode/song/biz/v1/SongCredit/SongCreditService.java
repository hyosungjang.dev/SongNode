package com.sparwk.songnode.song.biz.v1.SongCredit;

import com.sparwk.songnode.song.biz.v1.song.dto.SongVersionRequest;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditDetailResponse;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditProfileRequest;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditRequest;
import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongCreditBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongCredit;
import com.sparwk.songnode.song.jpa.entity.SongCreditInstrument;
import com.sparwk.songnode.song.jpa.repository.SongCreditInstrumentRepository;
import com.sparwk.songnode.song.jpa.repository.SongCreditRepository;
import com.sparwk.songnode.song.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongCreditService {
    // servlet
    private final HttpServletRequest request;

    // repository
    private final SongCreditRepository songCreditRepository;
    private final SongCreditInstrumentRepository songCreditInstrumentRepository;

    private final ModelMapper modelMapper;

    private final Logger logger = LoggerFactory.getLogger(SongCreditService.class);

    @Transactional
    public void saveSongCredit(SongCreditRequest songCreditRequest, Long songId) {
        logger.info("##### SONG_CREDIT_SERVICE : saveSongCredit >>>> {} ", songCreditRequest);
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        if(!StringUtils.isEmpty(songCreditRequest.getProfileList())) {
            Arrays.stream(songCreditRequest.getProfileList().split(",")).forEach(s -> {
                SongCreditBaseDTO songCreditBaseDTO = SongCreditBaseDTO.builder()
                        .songId(songId)
                        .profileId(Long.valueOf(s))
                        .deleteYn(CommonCodeConst.YN.N)
                        .regDt(LocalDateTime.now())
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .modDt(LocalDateTime.now())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .build();
                SongCredit entity = modelMapper.map(songCreditBaseDTO, SongCredit.class);
                songCreditRepository.save(entity);
            });
        }
    }

    /**
     * song 참여 회원 리스트
     * @param songId
     * @return
     */
    public List<SongCreditBaseDTO> selectBasicSongCredit(Long songId) {
        List<SongCreditBaseDTO> result = songCreditRepository
                .findBySongId(songId).stream()
                .map(e -> modelMapper.map(e, SongCreditBaseDTO.class))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * Song Credit List
     * @param songId
     * @return
     */
    public List<SongCreditDetailResponse> songCreditList(Long songId) {
        logger.info("##### SONG_CREDIT_SERVICE :songCreditList  songId >>>> {} ", songId);

        List<SongCreditDetailResponse> songCreditDetailResponses = new ArrayList<>();
        List<SongCredit> songcredit = songCreditRepository.findSongCreditsBySongId(songId);
        if(songcredit.size() > 0) {
            for (SongCredit songCredit : songcredit) {
                SongCreditDetailResponse result = selectSongCreditDetail(songCredit.getSongId(), songCredit.getProfileId());
                if(result != null){
                    songCreditDetailResponses.add(result);
                }
            }
        }
        return songCreditDetailResponses;
    }

    public SongCreditDetailResponse selectSongCreditDetail(Long songId, Long profileId){
        logger.info("##### selectSongCreditDetail ###");

        SongCreditDetailResponse songCreditDetailResponse = new SongCreditDetailResponse();
        SongCreditBaseDTO songCreditBaseDTO = songCreditOne(songId, profileId);

        StringBuilder roleList = new StringBuilder();
        if(songCreditBaseDTO != null) {
            List<SongCreditInstrument> roles = songCreditInstrumentRepository.findSongCreditInstrumentsBySongCreditSeq(songCreditBaseDTO.getSongCreditSeq());
            roles.forEach(songCreditInstrument -> {
                roleList.append(songCreditInstrument.getRoleCd().trim() + ",");
            });

            songCreditDetailResponse.setSongCreditResponse(songCreditBaseDTO);
            songCreditDetailResponse.setRoleCdList(roleList.toString());
        }

        return songCreditDetailResponse;
    }

    /**
     * 단일조회
     * @param songId
     * @param profileId
     * @return
     */
    public SongCreditBaseDTO songCreditOne(Long songId, Long profileId) {
        logger.info("##### SONG_CREDIT_SERVICE :songCreditOne");
        SongCredit vo = songCreditRepository.findSongCreditBySongIdAndProfileId(songId, profileId);
        SongCreditBaseDTO result = modelMapper.map(vo, SongCreditBaseDTO.class);
        return result;
    }

    /**
     * song credit
     * @param dto
     */
    @Transactional
    public void setProfileCredit(SongCreditProfileRequest dto) {
        logger.info("#### SERVICE :: IN<<<<< {}", dto);

        SongCreditBaseDTO songCreditBaseDTO = songCreditOne(dto.getSongId(), dto.getProfileId());
        Long seq = songCreditBaseDTO.getSongCreditSeq();

        SongCreditBaseDTO vo = modelMapper.map(dto, SongCreditBaseDTO.class);
        vo.setSongCreditSeq(seq);
        vo.setModUsr(dto.getOwnerProfileId());
        vo.setModDt(LocalDateTime.now());
        songCreditRepository.updateCreditInfo(vo);

        if(!StringUtils.isEmpty(dto.getRoleCdList())){
            songCreditInstrumentRepository.deleteSongCreditInstrumentsBySongCreditSeq(seq);
            Arrays.stream(dto.getRoleCdList().split(",")).forEach(s -> {
                SongCreditInstrument entity = SongCreditInstrument.builder()
                        .songCreditSeq(seq)
                        .roleCd(s)
                        .build();
                songCreditInstrumentRepository.save(entity);
            });
        }

    }

    /**
     * copy
     * @param vo
     */
    @Transactional
    public void songCreditCopy(SongVersionRequest vo){
        logger.info("#### song songCreditCopy ####");

        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        List<SongCreditBaseDTO> creditList = selectBasicSongCredit(vo.getSongId());

        if(creditList.size() > 0){
            creditList.stream().forEach(e -> {
                SongCredit songCredit = SongCredit.builder()
                        .songId(vo.getNewSongId())
                        .profileId(e.getProfileId())
                        .deleteYn(e.getDeleteYn())
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .regDt(LocalDateTime.now())
                        .modDt(LocalDateTime.now())
                        .build();
                songCreditRepository.save(songCredit);
            });
        }
    }


    /**
     * delete credit
     * @param songCreditRequest
     * @return
     */
    @Transactional
    public Long deleteSongCredit(SongCreditRequest songCreditRequest){
        logger.info("### SERVICE :: deleteSongCredit IN <<<< {}",songCreditRequest);
        Long songId = songCreditRequest.getSongId();
        Long profileId = Long.parseLong(songCreditRequest.getProfileList().replace(",","").trim());

        SongCredit entity = songCreditRepository.findSongCreditBySongIdAndProfileId(songId, profileId);
        SongCreditBaseDTO songCreditBaseDTO = modelMapper.map(entity, SongCreditBaseDTO.class);
        Long result = null;
        if(songCreditBaseDTO != null) {
            result = songCreditRepository.deleteSongCreditBySongCreditSeq(songCreditBaseDTO.getSongCreditSeq());
        }
        return result;
    }

    @Transactional
    public void deleteSongCreditAll(Long songId) {
        logger.info("#### SONG DELETE CREDIT ALL  IN <<<<< {}", songId);
        List<SongCreditBaseDTO> list = selectBasicSongCredit(songId);
        if(list.size() > 0) {
            list.stream().forEach(vo -> {
                songCreditRepository.deleteSongCreditBySongCreditSeq(vo.getSongCreditSeq());
                songCreditRepository.findSongCreditBySongIdAndProfileId(songId, vo.getProfileId());
            });
        }
    }

}
